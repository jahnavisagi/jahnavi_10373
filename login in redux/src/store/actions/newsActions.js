import * as allActions from '../actions/actionConstant';
export function receieveNews(data){
    
    console.log("datareceived");
    console.log(data);
    return {type:allActions.RECEIVE_NEWS,payload:data};

}
export function fetchNews(){
    console.log("fetching data");
    return{type:allActions.FETCH_NEWS,payload:{}};
 
    
}

export function receiveNewsById(data) {
    return { type: allActions.RECEIVE_NEWS_BY_ID, payload : data};
}

export function fetchNewsById(id) {
    return { type: allActions.FETCH_NEWS_BY_ID, payload : {id}};
}