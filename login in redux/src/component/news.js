import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsActions';
import counter from './counter';
import './news.css';
class News extends Component {

    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.newsActions.fetchNews();
    }
    render() {
        return (
            <React.Fragment>
                {
                   
                    (this.props.newsItems)
                        ? <div>
                            {this.props.newsItems.map(item => (
                                <div class="row">
                               
                                <div key={item._id}>
                                    <div class="container">
                                    <div class ="col-lg-12">
                                    <a href={`/news/${item._id}`}>{item._heading}View</a>
                                    {/* <a href={`/news/${item._id}`}>View</a> */}
                     <ul class ="header_item">
                     {/* <li><img src="https://img1.wsimg.com/isteam/ip/453e6e7b-2fa1-4acd-993d-0640b0382eeb/logo/6bda0679-f787-4c58-8ac2-5d071b697259.png/:/rs=h:75"style={{borderRadius:"25%",marginLeft:"5px"}}/></li> */}
                    
                     </ul>
                    
                     </div>
                                            <div class="col-lg-12" style={{ backgroundColor: "whitesmoke" }}>
                                                <h2 style={{ textAlign: "left" }}>{item.heading}</h2>

                                                <h5 class="header"><b> {item.brief}</b></h5>

                                                <p style={{ textAlign: "justify", color: "black" }}>{item.body}</p>
                                                <p style={{ textAlign: "right" }}><b>{item.tags}</b></p>
                                                <h6 style={{ textAlign: "left" }}><i>Comments:</i>{item.comments}</h6>
                                                <p style={{ textAlign: "left" }}> <i>Likes:</i>{item.likes.likes}</p>
                                                <p style={{ textAlign: "left" }}> <i>Dislikes:</i>{item.likes.dislikes}</p>
                                                <br></br>
                                                <p style={{ textAlign: "right" }}> <b>DateofPosted:</b>{item.timestamp}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                ))}
                        </div>
                        : <div>

                            loading.....
            </div>
                }
            </React.Fragment>
        );
    }

}
function mapStateToProps(state) {
    return {
        newsItems: state.news,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(News);