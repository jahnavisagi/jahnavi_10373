package com.student.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SourceServlet
 */
@WebServlet("/SourceServlet")
public class SourceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String pwd1 = request.getParameter("password");
		String pwd2 = request.getParameter("password1");
		
//		PrintWriter out = response.getWriter();
		
		List<String> books = new ArrayList<String> ();
		books.add("java");
		books.add(".net");
		books.add("c/c++");
		books.add("QE");
 		
		RequestDispatcher rd = request.getRequestDispatcher("display.jsp");
		
		if(pwd1.equals(pwd2)) {
			request.setAttribute("mybooks", books);
			rd.forward(request, response);
			 
		} else {
			response.sendRedirect("error.html");
		}
//		out.close();
	}

}
