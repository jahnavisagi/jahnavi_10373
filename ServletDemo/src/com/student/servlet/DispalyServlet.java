package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DispalyServlet
 */
@WebServlet("/DispalyServlet")
public class DispalyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	/**
	 * This method is used to get request from client in html format and it will send response in html form
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		int age=Integer.parseInt(request.getParameter("age"));
		//RequestDispatcher rd=request.getRequestDispatcher("Target");
		
		//HttpSession is used to maintain a unique session id for one person.
		HttpSession session=request.getSession();
		
		//used to display the session id
		String sid = session.getId();
		
		//print writer is initialized to print some text in html format
		PrintWriter pw=response.getWriter();
		pw.println("<html><body>");
		pw.println("Hello :"+name+"<br>");
		pw.println("Age is:"+age+"<br>");
		 session.setMaxInactiveInterval(10);
		
		pw.println("<a href='Target'>Click here to See your password</a>");
		pw.println("</body></html>");
		session.setAttribute("mypassword",password);
		
		
		
		//rd.forward(request,response);
		pw.close();
	}

	

}
