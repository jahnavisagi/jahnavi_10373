package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Target
 */
@WebServlet("/Target")
public class TargetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		    if(session!=null) {
			String sid = session.getId();
			String password = (String) session.getAttribute("mypassword");
			out.println("session id: "+sid+"<br>");
			out.println("Password :"+password);
			session.invalidate();
		    }
		    else {
		    	out.println("Authentication is invalid");
		    }
		out.println("</body></html>");
		out.close();
	}



}
