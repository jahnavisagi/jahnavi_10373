package com.ebank.service;

import java.util.List;

import com.ebank.beans.ComplaintsBean;

public interface ComplaintsService {
public boolean ariseComplaint(ComplaintsBean complaint);

public List<ComplaintsBean> fetchAllUserComplaints();



}
