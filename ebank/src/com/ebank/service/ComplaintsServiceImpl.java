package com.ebank.service;

import java.util.List;

import com.ebank.beans.ComplaintsBean;
import com.ebank.beans.UserBean;
import com.ebank.dao.ComplaintsDao;
import com.ebank.dao.ComplaintsDaoImpl;
import com.ebank.dao.UserDAO;
import com.ebank.dao.UserDaoImpl;

public class ComplaintsServiceImpl implements ComplaintsService{

	@Override
	public boolean ariseComplaint(ComplaintsBean complaint) {
		ComplaintsDao complaintDAO = new ComplaintsDaoImpl();
		boolean result = complaintDAO.writeComplaintIntoTable(complaint);
		return result;
		
	}
	// this method is used to display all the users in the table
		@Override
		public List<ComplaintsBean> fetchAllUserComplaints() {
			ComplaintsDao complaintsDAO = new ComplaintsDaoImpl();
			List<ComplaintsBean> complaintsList = complaintsDAO.getAllUserComplaints();
			return complaintsList;
		}
	

}
