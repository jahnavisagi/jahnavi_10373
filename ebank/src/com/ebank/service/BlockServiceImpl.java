package com.ebank.service;

import com.ebank.dao.BlockingDao;
import com.ebank.dao.BlockingDaoImpl;

public class BlockServiceImpl implements BlockingService{

	@Override
	public boolean isBlocked(int accountNumber) {
		BlockingDao blocking=new BlockingDaoImpl();
		boolean result=blocking.isUserBlocked(accountNumber);
		return result;
	}

	@Override
	public boolean blockOrUnblockUser(int accountNumber) {
		boolean flag=false;
		
		
		
		BlockingDao blocking=new BlockingDaoImpl();
		
		boolean result=blocking.updateUserStatus(accountNumber, flag);
		return result;
	}

	public void displayUserStatus() {
		// TODO Auto-generated method stub
		
	}

}
