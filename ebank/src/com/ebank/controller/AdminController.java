package com.ebank.controller;

import com.ebank.beans.AdminBean;

public interface AdminController{
	
	/**
	 * This method is used for registering new Admin into AdminDetails Table
	 */
	void adminRegistration();
	
	/**
	 * This method is used for Logging in Admin by using adminId and password
	 */
	void Login();
	
	/**
	 * This method is used to perform Admin Operations after logging in
	 * @param adminId
	 */
	void printAfterLoginPage(int adminId);
	
	/**
	 * This method is used for navigation between login and registration of admin
	 */
	void adminNavigator();
	
	/**
	 * This method is used to display all the users present in the user table.
	 */
	void displayUserDetails();
	
	/**
	 * This method is used to update the details of admin in adminDetails table.
	 * @param adminId
	 * @return
	 */
	AdminBean updateAdminDetails(int adminId);
}
