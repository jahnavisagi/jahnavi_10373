package com.ebank.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ebank.EBanking;
import com.ebank.beans.AccountBean;
import com.ebank.beans.AdminBean;
import com.ebank.beans.ComplaintsBean;
import com.ebank.beans.UserBean;
import com.ebank.beans.UserTransactionBean;
import com.ebank.service.AdminService;
import com.ebank.service.AdminServiceImpl;
import com.ebank.service.ComplaintsService;
import com.ebank.service.ComplaintsServiceImpl;
import com.ebank.service.TransactionServiceImpl;
import com.ebank.service.UserService;
import com.ebank.service.UserServiceImpl;
import com.ebank.utility.DateReader;
import com.ebank.utility.UserInputReader;
import com.ebank.utility.UserInputReaderImpl;
import com.ebank.utility.Validations;

public class UserControllerImpl implements UserController {
	int user_id;
	// creating reference object for userbean class and user service class
	UserBean user = new UserBean();
	UserService userService = new UserServiceImpl();

	/**
	 * this method is used for registering new user
	 */
	public void userRegistration() {

		UserService userservice = new UserServiceImpl();
		Validations validate = new Validations();
		UserBean user = new UserBean();
		AccountBean account = new AccountBean();
		boolean flag = false;
		Scanner sc = new Scanner(System.in);
		try {
			// while (!flag) {
			System.out.println("Enter userid");
			int userId = sc.nextInt();
			account.setUserId(userId);

			user.setUserId(userId);
			System.out.println("Enter AccountNumber");
			account.setAccountNumber(sc.nextInt());
//TODO validate userId
			// }
			flag = false;
			while (!flag) {
				System.out.println("Enter First name");
				user.setFirstName(sc.next());
				flag = validate.nameValidator(user.getFirstName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter last name");
				user.setLastName(sc.next());
				flag = validate.nameValidator(user.getLastName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter password");
				user.setPassword(sc.next());
				flag = validate.userPasswordValidate(user.getPassword());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter phoneNumber");
				user.setPhoneNum(sc.next());
				flag = validate.phoneNumValidator(user.getPhoneNum());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter mailId");
				user.setMailId(sc.next());
				flag = validate.emailValidator(user.getMailId());
			}

			System.out.println("Enter DateOfBirth");
			try {
				user.setDateOfBirth(new SimpleDateFormat("dd/mm/yyyy").parse(sc.next()));
				System.out.println(user.getDateOfBirth());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			System.out.println("Enter Gender(f/m)");
			user.setGender(sc.next());

			flag = false;
			while (!flag) {
				System.out.println("Enter Account name");
				account.setAccountHolderName(sc.next());
				flag = validate.nameValidator(account.getAccountHolderName());
			}
			flag = false;
			while (!flag) {
				System.out.println("Enter Account Nominee name");
				account.setNomineeName(sc.next());
				flag = validate.nameValidator(account.getNomineeName());
			}

			System.out.println("Enter Account Balance");
			account.setBalance(sc.nextDouble());

			account.setOpeningDate(Calendar.getInstance().getTime());

			account.setStatus(true);
			flag = false;

			while (!flag) {
				System.out.println("Enter TypeOfAccount");
				account.setTypeOfAccount(sc.next());
				flag = validate.nameValidator(account.getTypeOfAccount());
			}

			// returns true if registration is done successfully
			boolean result = userservice.doRegistrationUser(user);

			if (result) {
				AdminBean admin = new AdminBean();
				System.out.println("records saved");
				boolean result1 = userservice.doRegistrationAccount(account);
				AdminController adminController = new AdminControllerImpl();
				adminController.printAfterLoginPage(admin.getAdminId());
				System.out.println("-----------------------------------");
			} else {
				System.out.println("Something went wrong");
				System.out.println("-----------------------------------");
			}
		} catch (Exception e) {
			System.out.println("Invalid data");
			System.out.println("-----------------------------------");
		}
	}

	/**
	 * this method is used for user login
	 */
	public void userLogin() {
		// initializing the scanner class
		Scanner sc = new Scanner(System.in);
		UserService userservice = new UserServiceImpl();
		UserBean user = new UserBean();

		// login of user
		System.out.println("enter userId:");
		int userId = sc.nextInt();
		// returns true if userid is present in the database
		boolean login1 = userservice.doLogin(userId);
		if (login1) {
			System.out.println("Enter the Password:");
			String password1 = sc.next();
			// returns true if both userid and password are present in the table
			boolean login2 = userservice.doLogin(userId, password1);
			if (login2) {
				System.out.println("Successfull Login");
				System.out.println("***************************");
				user = userservice.getUserById(userId);
				user_id = user.getUserId();
				bankOperations(userId);
			} else {
				System.out.println("Incorrect Password");
				System.out.println("Do you want to reset your password\nyes or no");
				String reset = sc.next();
				if (reset.equals("yes")) {

					System.out.println("Enter the Password:");
					String password = sc.next();

					boolean result = userservice.updatePassword(userId, password);
					this.userLogin();

				} else {
					System.out.println("try again");
					this.userLogin();
				}
			}

		}

		else {

			System.out.println("you are not registered");
			System.out.println("-----------------------------------");
			this.userLogin();
		}
	}

	/**
	 * This method is used to update user details
	 * 
	 * @param user
	 * @return UserBean object
	 */
	public UserBean updateUserDetails(int userId2) {
		UserInputReader inputReader = new UserInputReaderImpl();
		AdminService adminService = new AdminServiceImpl();
		user = userService.getUserById(userId2);
		int userId = user.getUserId();
		System.out.println(userId);
		UserService userService = new UserServiceImpl();
		user = userService.getUserById(userId);
		if (user != null) {
			System.out.println("userDetails\n-------------------------\nuserId:" + user.getUserId() + "\nFirstname: "
					+ user.getFirstName() + "\nLastName: " + user.getLastName() + "\nPassword: " + user.getPassword()
					+ "\nPhoneNumber: " + user.getPhoneNum() + "\nEmailId: " + user.getMailId() + "\nDateOfBirth: "
					+ user.getDateOfBirth() + "\nGender: " + user.getGender());
		} else {
			System.out.println("No record found");
		}
		try {
			boolean flag = true;
			boolean result = false;
			while (flag) {
				System.out.println(
						"1)FirstName\n2)LastName\n3)password\n4)phoneNum\n5)mailId\n6)dateOfBirth\n7)gender\n8)update\nEnter your choice");
				String input = inputReader.readString();
				switch (input) {
				case "1":
					System.out.println("enter firstname");
					String firstName = inputReader.readString();
					result = userService.updateFirstName(userId2, firstName);
					break;
				case "2":
					System.out.println("enter lastname");
					String lastName = inputReader.readString();
					result = userService.updateLastName(userId, lastName);
					break;
				case "3":
					System.out.println("enter password");
					String password = inputReader.readString();
					result = userService.updatePassword(userId, password);
					break;
				case "4":
					System.out.println("enter phone Number");
					String phoneNum = inputReader.readString();
					result = userService.updatePhoneNumber(userId, phoneNum);
					break;
				case "5":
					System.out.println("enter mailId");
					String mailId = inputReader.readString();
					result = userService.updateMailId(userId, mailId);
					break;
				case "6":
					System.out.println("enter dateOfBirth");
					Date dateOfBirth = new DateReader().readDate();
					result = userService.updateDateOfBirth(userId, dateOfBirth);
					break;
				case "7":
					System.out.println("enter gender(F/M)");
					String gender = String.valueOf(inputReader.readChar());
					result = userService.updateGender(userId, gender);
					break;
				case "8":
					flag = false;

					break;
				default:
					System.out.println("you choose invalid option");
					break;
				}
			}
			if (result) {
				System.out.println("**********Successfully updated.************");
				int u = user.getUserId();
				bankOperations(user.getUserId());

			} else {
				System.out.println("something went wrong");
				this.updateUserDetails(userId2);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	} // end of updateUserDetails

	/**
	 * This method retrieves UserBean object based on userId and performs updations
	 * on that object.
	 * 
	 * @param userId
	 */

	public void bankOperations(int userId) {
		Scanner sc = new Scanner(System.in);

		UserBean user = new UserBean();
		boolean flag = true;
		int accNum = new UserServiceImpl().getAccountdetails(userId).getAccountNumber();
		while (flag) {
			System.out.println(
					"1.BalanceEnquiry\n2.Deposit\n3.Withdraw\n4.TransferMoney\n5.MiniStatement\n6.TransactionHistory\n7.BillPayment\n8.UpdateDetails\n9.AriseComplaint\n10.DisplayUserInfo\n11.Exit");
			System.out.println("Enter your choice");
			String choice = sc.next();

			switch (choice) {
			case "1":
				doBalanceEnquiry(accNum);
				break;
			case "2":
				doDeposit(accNum);
				break;
			case "3":
				doWithdraw(accNum);
				break;
			case "4":
				doTransfer(accNum);
				break;
			case "5":
				getMiniStatement(accNum);
				break;
			case "6":
				getTransactionHistory(accNum);
				break;
			case "7":
				billPayment(accNum);
				break;
			case "8":
				updateUserDetails(userId);
				break;
			case "9":
				ariseComplaint(accNum,userId);
				break;
			case "10":
				UserService userService = new UserServiceImpl();
				user = userService.getUserById(userId);
				if (user != null) {
					System.out.println("\nUserId:-" + user.getUserId() + "\nFirstName:- " + user.getFirstName()
							+ "\nLastName:-  " + user.getLastName() + " \nMailId:- " + user.getMailId()
							+ "\nPassword:-  " + user.getPassword() + "\nPhone Number:-  " + user.getPhoneNum()
							+ " \nDateOfBirth:- " + user.getDateOfBirth()
							+ "\n------------------------------------------------\n");
				} else {
					System.out.println("No record found");
				}
				break;
			case "11":
				flag = false;
				System.out.println("You have successfully logged out");
				new EBanking().startApp();
				break;
			default:
				System.out.println("Enter a valid input");

			}
		}
	}

	/**
	 * This method used to perform deposit operation
	 */
	@Override
	public void doDeposit(int accNum) {
		UserInputReader inputReader = new UserInputReaderImpl();
		// asking user to enter amount
		System.out.println("please enter amount to be deposit");
		double amount = inputReader.readDouble();
		UserTransactionBean txBean = new UserTransactionBean();
		System.out.println("enter reason of transaction.");
		txBean.setMessage(inputReader.readString());
		// calling service deposit method
		boolean status = new TransactionServiceImpl().doDeposit(amount, accNum, txBean);
		if (status) {
			System.out.println("deposit is sucess..");
		} else {
			System.out.println("deposit is faliled..");
		}
		doBalanceEnquiry(accNum);
	} // End of doDeposit

	/**
	 * This method performs withdraw operation
	 */
	@Override
	public void doWithdraw(int accNum) {
		UserInputReader inputReader = new UserInputReaderImpl();
		// asking user to enter amount
		System.out.println("please enter amount for transaction");
		double amount = inputReader.readDouble();
		UserTransactionBean txBean = new UserTransactionBean();
		System.out.println("enter reason of transaction.");
		txBean.setMessage(inputReader.readString());
		// calling service withdraw method
		boolean status = new TransactionServiceImpl().doWithdraw(amount, accNum, txBean);
		if (status) {
			System.out.println("Transaction sucess...");
		} else {
			System.out.println("transaction failed..");
		}
		doBalanceEnquiry(accNum);
	} // End of doWithdraw

	/**
	 * This method performs amount transfer operation
	 */
	@Override
	public void doTransfer(int fromAccNum) {
		// TODO check account exist or not
		UserInputReader inputReader = new UserInputReaderImpl();
		System.out.println("please enter account number to which amount has to transfer");
		int toAccNum = inputReader.readInt();
		System.out.println("please enter amount to be transefer");
		double amount = inputReader.readDouble();
		UserTransactionBean txBean = new UserTransactionBean();
		System.out.println("enter reason of transaction.");
		txBean.setMessage(inputReader.readString());
		boolean status = new TransactionServiceImpl().doTransferAmount(fromAccNum, toAccNum, amount, txBean);
		if (status) {
			System.out.println("transfer is sucess...");
		} else {
			System.out.println("transfer is failed..");
		}
		doBalanceEnquiry(fromAccNum);
	} // End of doTransfer

	/**
	 * This is implementation of UserController getTransactionHistory(int) method.
	 * This method prints transaction history of user for particular account number
	 */
	@Override
	public void getTransactionHistory(int accNum) {
		List<UserTransactionBean> txList = new TransactionServiceImpl().transactionHistory(accNum);
		System.out.println();
		System.out.println("Transaction History for Account Number :" + accNum);
		System.out.println("****************************************************");
		printTransaction(txList);

	} // End of getTransactionHistory

	/**
	 * This method is implementation of UserController
	 * printTransaction(List<UserTransactionBean>) method. This method contains
	 * functionality to print List type object which contains transaction objects
	 */
	@Override
	public void printTransaction(List<UserTransactionBean> txList) {
		for (UserTransactionBean bean : txList) {
			System.out.println("TransactionId :" + bean.getTransactionId());
			System.out.println("Date Of transaction :" + bean.getTransactionDate());
			System.out.println("Debit Amount :" + bean.getDebitAmount());
			System.out.println("Credit Amount :" + bean.getCreditAmount());
			System.out.println("Status of Transaction :" + bean.getStatus());
			System.out.println("Narration :" + bean.getMessage());
			System.out.println("----------------------------------------");
		}
	} // End of printTransaction

	/**
	 * This method performs miniStatement operation which prints last five
	 * transactions of account
	 */
	@Override
	public void getMiniStatement(int accNum) {
		List<UserTransactionBean> txList = new TransactionServiceImpl().miniStatement(accNum);
		System.out.println();
		System.out.println("MiniStatement for Account Number :" + accNum);
		System.out.println("****************************************************");
		printTransaction(txList);

	} // End of getMiniStatement

	/**
	 * This is implementation of UserController doBalanceEnquiry(int) method. This
	 * performs balance inquiry operation of specific account.
	 */
	@Override
	public void doBalanceEnquiry(int accNum) {
		System.out.println("Balance in your account is :" + new TransactionServiceImpl().doBalEnq(accNum));
	} // End of doBalanceEnquiry

	public void billPayment(int accNum) {
		UserBean user = new UserBean();
		UserInputReader inputReader = new UserInputReaderImpl();
		boolean flag = true;
		while (flag) {
			System.out.println("1.currentBill\n2.Waterbill\n3.phonebill\n4.exit\nenter your choice:");
			String choice = inputReader.readString();
			switch (choice) {
			case "1":
				doWithdraw(accNum);
				break;
			case "2":
				doWithdraw(accNum);
				break;
			case "3":
				doWithdraw(accNum);
				break;
			case "4":
				flag = false;
				break;
			default:
				System.out.println("enter a valid choice");
			}
		} // end of while
		this.bankOperations(user.getUserId());
	}// end of billPayment method

	/**
	 * This method asks user to write complaint and calls repository method
	 * 
	 * @param accNum
	 */
	public void ariseComplaint(int accNum,int userId) {
		Scanner sc = new Scanner(System.in);
		ComplaintsBean complaint = new ComplaintsBean();
		ComplaintsService complaintService = new ComplaintsServiceImpl();
		System.out.println("please, write your complaint :");
		complaint.setDescription(sc.next());
		//System.out.println("Enter userId");
		complaint.setUserId(userId);
		Timestamp date = new Timestamp(new java.util.Date().getTime());
		complaint.setDateOfComplaint(date);
		boolean flag = complaintService.ariseComplaint(complaint);
		if (flag) {
			System.out.println("Complaint is successfully sent");
			//bankOperations(userId);
		} else {
			System.out.println("Something went wrong");
		}
	} // End of ariseComplaint

	
}
