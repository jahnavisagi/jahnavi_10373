package com.ebank.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.ebank.EBanking;
import com.ebank.beans.AdminBean;
import com.ebank.beans.ComplaintsBean;
import com.ebank.beans.UserBean;
import com.ebank.service.AdminService;
import com.ebank.service.AdminServiceImpl;
import com.ebank.service.BlockServiceImpl;
import com.ebank.service.ComplaintsService;
import com.ebank.service.ComplaintsServiceImpl;
import com.ebank.service.UserService;
import com.ebank.service.UserServiceImpl;
import com.ebank.utility.DateReader;
import com.ebank.utility.UserInputReader;
import com.ebank.utility.UserInputReaderImpl;
import com.ebank.utility.Validations;

public class AdminControllerImpl implements AdminController {
	//creating the reference object for adminbean class and adminservice class
	AdminBean admin = new AdminBean(); 
	AdminService adminService = new AdminServiceImpl();
	int admin_id;
    /**
     * This method is used for navigation between registering and login of admin
     */
	public void adminNavigator() {
		UserInputReader inputReader = new UserInputReaderImpl();
		System.out.println("1.Registration\n2.Login\nEnter your choice");
		String choice=inputReader.readString();
		switch(choice) {
		case "1":
			adminRegistration();
			break;
		case "2":
			Login();
			break;
		default:
			System.out.println("Please enter a valid choice");
		}
	}
	
	/**
	 * This method is used for registering new admin into database
	 */
	public void adminRegistration() {
		AdminService adminService=new AdminServiceImpl();
		AdminBean admin=new AdminBean();
		Validations validate = new Validations();
		UserInputReader inputReader = new UserInputReaderImpl();
		boolean flag=false;
//		while(!flag) {
			System.out.println("Enter AdminId");
			admin.setAdminId(inputReader.readInt());
			
//TODO adminid validator
//		}
		flag=false;
		while(!flag) {
			System.out.println("Enter Admin name");
			admin.setAdminName(inputReader.readString());
			flag=validate.nameValidator(admin.getAdminName());
		}
		flag=false;
		while(!flag) {
			System.out.println("Enter Admin password");
			admin.setPassword(inputReader.readString());
			flag=validate.adminPasswordValidate(admin.getPassword());
		}
//		flag=false;
//		while(!flag) {
		System.out.println("Enter Gender(m/f)");
		admin.setGender(inputReader.readString());
//TODO gender validation
//		}
//		flag=false;
//		while(!flag) {
			System.out.println("Enter DateOfBirth"); 
			try {
				admin.setDateOfBirth(new SimpleDateFormat("dd/mm/yyyy").parse(inputReader.readString()));
				System.out.println(admin.getDateOfBirth());
			} catch (ParseException e) {
			
			e.printStackTrace();
//TODO DOB validation
//		}
		}
		flag=false;
		while(!flag) {
			System.out.println("Enter mailId");
			admin.setMailId(inputReader.readString());
			flag=validate.emailValidator(admin.getMailId());
		}
		flag=false;
		while(!flag) {
			System.out.println("Enter phone number");
			admin.setPhoneNum(inputReader.readString());
			flag=validate.phoneNumValidator(admin.getPhoneNum());
		}
		//returns true if registration is successfull
			boolean result = adminService.doRegistration(admin);
		if (result) {
			System.out.println("records saved");
			System.out.println("======================");
			adminNavigator();
		}
		else {
			System.out.println("Something went wrong");
		}
	}

	/**
	 * This method is used for logging in admin using logging credentials
	 */
	public void Login() {
 		Scanner sc = new Scanner(System.in);
		AdminService adminservice = new AdminServiceImpl();
		AdminBean admin = new AdminBean();

		// login of user
		System.out.println("enter adminId:");
		int adminId = sc.nextInt();	
		//returns true if adminid is present in the database
		boolean login1 = adminservice.doLogin(adminId);
		if(login1) {
			System.out.println("Enter the Password:");
			String password1 = sc.next();
			//returns true if adminid and password is present in the database
			boolean login2 = adminservice.doLogin(adminId,password1);
			if(login2) {
				System.out.println("Successfull Login");
				admin = adminService.getAdminById(adminId);
				admin_id = admin.getAdminId();
				printAfterLoginPage(adminId);
				
			}
			else {
				System.out.println("Incorrect Password");
				System.out.println("Do you want to reset your password\nyes or no");
				String reset = sc.next();
				if(reset.equals("yes")) {
					
					System.out.println("Enter the Password:");
					String password = sc.next();
					
				
						boolean result = adminservice.updatePassword(adminId, password);
						if(result) {
							System.out.println("Password has changed");
							System.out.println("-------------------------------");
							printAfterLoginPage(adminId);
						}
						
					}
				else {
					System.out.println("Try again");
					adminNavigator();
				}
				
					}
				
			}
			
		
		else {
		
			System.out.println("you are not registered");
			System.out.println("--------------------------------\n\n");
			this.Login();
		}
	}
	
	
	/**
	 * This method is used to update admin details
	 * 
	 * @param adminId2
	 * @return AdminBean object
	 */
	public AdminBean updateAdminDetails(int adminId2) {
		UserInputReader inputReader = new UserInputReaderImpl();
		AdminService adminService = new AdminServiceImpl();
		admin = adminService.getAdminById(adminId2);
		int adminId = admin.getAdminId();
		try {
			boolean flag = true;
			boolean result= false;
			while (flag) {
				if(admin!=null) {
					System.out.println("AdminDetails\n-------------------------\nAdminId:-"+admin.getAdminId()+"\nAdminName:-"+admin.getAdminName()+"\nPassword:-"+admin.getPassword()+"\nGender:-"+admin.getGender()+"\nMailId:-"+admin.getMailId()+"\nPhoneNum:-"+admin.getPhoneNum()+"\nDateOfBirth:-"+admin.getDateOfBirth()+"\n**************************");
				}
				System.out.println("1)adminName\n2)password\n3)gender\n4)dateOfBirth\n5)mailId\n6)phoneNum\n7)update");
				String input = inputReader.readString();
				System.out.println("==================");
				switch (input) {
				//admin name 
				case "1":
					System.out.println("enter name");
					String name=inputReader.readString();
					result = adminService.updateName(adminId, name);
					break;
				case "2":
					System.out.println("enter password");
					String password = inputReader.readString();
					result = adminService.updatePassword(adminId, password);
					break;
				case "3":
					System.out.println("enter gender(m/f)");
					String gender = String.valueOf(inputReader.readChar());
					result = adminService.updateGender(adminId, gender);
					break;

				case "4":
					System.out.println("enter dateOfBirth");
					Date dateOfBirth = new DateReader().readDate();
					result = adminService.updateDate(adminId, dateOfBirth);
					break;
				case "5":
					System.out.println("enter mailId");
					String mailId = inputReader.readString();
					result = adminService.updateMailId(adminId, mailId);
					break;
				case "6":
					System.out.println("enter phone Number");
					String phoneNum = inputReader.readString();
					result = adminService.updatePhoneNumber(adminId, phoneNum);
					break;
				case "7":
					flag = false;
					break;
				default:
					System.out.println("you choose invalid option");
					break;
				}
			}
			
			if (result) {
				System.out.println("successfully updated.");
				printAfterLoginPage(adminId);
			} else {
				System.out.println("something went wrong");
				this.updateAdminDetails(adminId2);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return admin;
	} // end of updateAdminDetails

	
	
	/**
	 * This method is an utility method prints content after Admin Login.
	 */
	public void printAfterLoginPage(int adminId) {
		// initializing the scanner class
		Scanner sc = new Scanner(System.in);
		AdminBean admin=new AdminBean();
		boolean flag = true;
		while (flag) {
			System.out.println("-------------------------------------------");
			System.out.println("1)add new User");
			System.out.println("2)Update Admin Details");
			System.out.println("3)display User Details");
			System.out.println("4)Block Or UNBlock user");
			System.out.println("5)display usercomplaints");
			System.out.println("6)admin Logout");

			String choice = sc.next();
			
				switch (choice) {
				case "1":
					UserControllerImpl user=new UserControllerImpl();
					user.userRegistration();
					
					break;
					
				case "2":
					updateAdminDetails(adminId);
					
					break;
				case "3":
					displayUserDetails();
					
					break;
				case "4":
					UserBean user1=new UserBean();
					int userId = user1.getUserId();
					UserService userService = new UserServiceImpl();
					user1 = userService.getUserById(userId);
					System.out.println("Enter account number to block or unblock user");
					int accnum=sc.nextInt();
					
					boolean userflag=new UserServiceImpl().doLogin(userId);
					if(userflag) {
						new BlockServiceImpl().blockOrUnblockUser(accnum);
					}
					else {
						System.out.println("given account number doesnot exists");
					}
					break;
					
				case "5":
					displayUserComplaints();
					break;
				case "6":
					System.out.println("You have logged out successfully");
					new EBanking().startApp();
					flag = false;
					break;

				default:
					System.out.println("Please enter a valid choice");
				}
			
		}
	} // end of printAfterLoginPage
	/**
	 * This method displays the user records from table
	 */
	public void displayUserDetails(){
		UserService userService = new UserServiceImpl();
		List<UserBean> userList = userService.fetchAllUsers();
		Iterator<UserBean> it1 = userList.iterator();  
		
		while(it1.hasNext()){  			
			UserBean userList1 = (UserBean) it1.next();
			System.out.println("**********UserDetails***********");
			System.out.println("\nUserId:-"+userList1.getUserId()+ "\nFirstName:- " +userList1.getFirstName()+ "\nLastName:-  "+ userList1.getLastName()+ " \nMailId:- "+ userList1.getMailId()+ "\nPassword:-  "+ userList1.getPassword()+ "\nPhone Number:-  "+ userList1.getPhoneNum()+ " \nDateOfBirth:- "+ userList1.getDateOfBirth()+"\n------------------------------------------------\n"); 
			AdminBean admin = new AdminBean();
			
			
		}
		printAfterLoginPage(admin.getAdminId());
	}
	/**
	 * This method displays the user complaints  from table
	 */
	public void displayUserComplaints(){
		ComplaintsService complaintsService = new ComplaintsServiceImpl();
		List<ComplaintsBean> complaintsList = complaintsService.fetchAllUserComplaints();
		Iterator<ComplaintsBean> it1 = complaintsList.iterator();  
		
		while(it1.hasNext()){  			
			ComplaintsBean complaintsList1 = (ComplaintsBean) it1.next();
			System.out.println("**********User Complaints**********");
			System.out.println("\nComplaintId:-"+complaintsList1.getComplaintId()+ "\nUserId:- " +complaintsList1.getUserId()+ "\nDescription:-  "+ complaintsList1.getDescription()+ " \nDateOfComplaint:- "+ complaintsList1.getDateOfComplaint()+"\n------------------------------------------------\n"); 
			AdminBean admin = new AdminBean();
			
			
		}
		printAfterLoginPage(admin.getAdminId());
	}


	
}
