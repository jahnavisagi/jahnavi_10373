package com.ebank.dao;

import java.util.Date;

import com.ebank.beans.AdminBean;

/**
 * This interface contains method declarations which deals with admin related
 * database table
 * 
 * @author Team-E
 *
 */
public interface AdminDAO {
	/**
	 * This method checks whether record exist or not
	 * 
	 * @param adminId
	 * @return true if record exist else false
	 */
	boolean isRecordExist(int adminId);

	/**
	 * This method checks whether record exist or not
	 * 
	 * @param adminId
	 * @param password
	 * @return true if record exist else false
	 */
	boolean isRecordExist(int adminId, String password);
	
	/**
	 * This method fetches record based on primary key column
	 * @param adminId
	 * @return AdminBean
	 */
	AdminBean fetchRecordById(int adminId);

	/**
	 * This method inserts an admin record into the data base table
	 * 
	 * @param admin
	 * @return return true if success else false
	 */
	boolean insertAdminRecord(AdminBean admin);

	/**
	 * This method updates existing admin record in the data base table
	 * 
	 * @param admin
	 * @return true if success else false
	 */
	boolean updateAdminRecord(AdminBean admin);
	public boolean updatePassword(int userId,String password) ;
	
	/**
	 * This method is used for logging in admin
	 * @param adminId
	 * @return true if adminid is present in the record
	 */
	public boolean doLogin(int adminId);

	/**
	 * This method is used to update name of admin
	 * @param adminId
	 * @param name
	 * @return true if name is updated in database
	 */
	boolean updateName(int adminId, String name);

	/**
	 * This method is used to update dataOfBirth of admin
	 * @param adminId
	 * @param dateOfBirth
	 * @return  true if dataOfBirth is updated in database
	 */
	boolean updateDate(int adminId, Date dateOfBirth);

	/**
	 * This method is used to update gender of admin
	 * @param adminId
	 * @param gender
	 * @return  true if gender is updated in database
	 */
	boolean updateGender(int adminId, String gender);

	/**
	 * This method is used to update mailId of admin
	 * @param adminId
	 * @param mailId
	 * @return true if mailId is updated in database
	 */
	boolean updateMailId(int adminId, String mailId);

	/**
	 * This method is used to update phoneNum of admin
	 * @param adminId
	 * @param phoneNum
	 * @return true if phoneNum is updated in database
	 */
	boolean updatePhoneNumber(int adminId, String phoneNum);
}
