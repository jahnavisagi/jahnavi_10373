package com.ebank.dao;

import java.util.Date;
import java.util.List;

import com.ebank.beans.AccountBean;
import com.ebank.beans.AdminBean;
import com.ebank.beans.UserBean;

/**
 * This interface contains method declarations which deals with user related
 * database table
 * 
 * @author Team-E
 *
 */
public interface UserDAO {
	/**
	 * This method checks whether record exist or not
	 * 
	 * @param userId
	 * @param password
	 * @return true if record exist else false
	 */
	boolean isRecordExists(int userId, String Password);

	/**
	 * This method checks whether record exist or not
	 * 
	 * @param userId
	 * @return true if record exist else false
	 */
	boolean doLogin(int userId);

	/**
	 * This method inserts an account record into the data base table
	 * 
	 * @param account
	 * @return return true if success else false
	 */
	boolean insertAccountRecord(AccountBean account);

	/**
	 * This method inserts an user record into the data base table
	 * 
	 * @param user
	 * @return return true if success else false
	 */
	boolean insertUserRecord(UserBean user);

	/**
	 * This method will display all the users present in the table
	 * 
	 * @return list of users
	 */
	public List<UserBean> getAllUsers();

	/**
	 * This method fetches record based on primary key column
	 * 
	 * @param userId
	 * @return UserBean
	 */
	UserBean fetchRecordById(int userId);

	/**
	 * This method will update the password of the user
	 * 
	 * @param userId
	 * @param password
	 * @return true if Password is updated in database
	 */
	public boolean updatePassword(int userId, String password);

	/**
	 * 
	 * This method will update the firstName of the user
	 * 
	 * @param userId
	 * @param firstName
	 * @return true if firstName is updated in database
	 */
	boolean updateFirstName(int userId, String firstName);

	/**
	 * This method will update the lastName of the user
	 * 
	 * @param userId
	 * @param lastName
	 * @return true if LastName is updated in database
	 */
	boolean updateLastName(int userId, String lastName);

	/**
	 * This method will update the phoneNum of the user
	 * 
	 * @param userId
	 * @param phoneNum
	 * @return true if phoneNum is updated in database
	 */
	boolean updatePhoneNumber(int userId, String phoneNum);

	/**
	 * This method will update the mailId of the user
	 * 
	 * @param userId
	 * @param mailId
	 * @return true if mailId is updated in database
	 */
	boolean updateMailId(int userId, String mailId);

	/**
	 * 
	 * @param userId
	 * @param dateOfBirth
	 * @return true if Password is updated in database
	 */
	boolean updateDateOfBirth(int userId, Date dateOfBirth);

	/**
	 * This method will update the gender of the user
	 * 
	 * @param userId
	 * @param gender
	 * @return true if gender is updated in database
	 */
	boolean updateGender(int userId, String gender);

	/**
	 * This method used to fetch account details based on userId
	 * 
	 * @param userId
	 * @return
	 */
	AccountBean fetchAccountByUserId(int userId);

}
