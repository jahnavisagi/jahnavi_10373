package com.ebank.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ebank.beans.ComplaintsBean;
import com.ebank.beans.UserBean;
import com.ebank.utility.DButil;

public class ComplaintsDaoImpl implements ComplaintsDao{
	ComplaintsBean complaints=new ComplaintsBean();
	/**
	 * This is implementation of ComplaintsDao
	 * insertComplaint(ComplaintsBean) method
	 */
	
	@Override
	public boolean writeComplaintIntoTable(ComplaintsBean complaint) {
		
		Connection con = null;
		PreparedStatement ps = null;
		int rowsEffected = 0;
		// preparing sql query for
		String sql = "insert into complaints(userId,description,dateOfComplaint) values(?,?,?)";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, complaint.getUserId());
			ps.setString(2,complaint.getDescription());
			ps.setTimestamp(3, complaint.getDateOfComplaint());
			
			
			// executing sql query
			rowsEffected = ps.executeUpdate();
			
		} catch (Exception e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (rowsEffected > 0) ? true : false;
	}
	
	/**
	 * This method fetches transaction details from database tables and returns list
	 * of transactions
	 */
	@Override
	public List<ComplaintsBean> fetchTransactions(String complaint,int accNum) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ComplaintsBean> complaintList = new ArrayList<>();
		// preparing sql query for
		String sql = "select complaintId,userId,description,dateOfComplaint from complaints where accountNum = ?";
		try {
			// getting connection object
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accNum);

			// executing sql query
			rs = ps.executeQuery();
			while (rs.next()) {
				// creating UserTransactionBean object
				ComplaintsBean complaints = new ComplaintsBean();
				// setting values to the bean object from resultset
				complaints.setComplaintId(rs.getInt(1));
				complaints.setUserId(rs.getInt(2));
				complaints.setDateOfComplaint(rs.getTimestamp(3));
				complaints.setDescription(rs.getString(4));
				
				// adding bean object to the list
				complaintList.add(complaints);
			} // End of while
		} catch (Exception e) {

		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaintList;
	} // End of fetchTransactions

	@Override
	public List<ComplaintsBean> getAllUserComplaints() {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ComplaintsBean complaints = null;
		List<ComplaintsBean> userlist = new ArrayList<ComplaintsBean>();
		// this query is used to display all the users present in the user table
		String sql = "select * from complaints ";

		try {
			con = DButil.getcon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				complaints = new ComplaintsBean();
				complaints.setUserId(rs.getInt(2));
				complaints.setComplaintId(rs.getInt(1));
				complaints.setDescription(rs.getString(3));
				complaints.setDateOfComplaint(rs.getTimestamp(4));
				userlist.add(complaints);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return userlist;
	}
}
