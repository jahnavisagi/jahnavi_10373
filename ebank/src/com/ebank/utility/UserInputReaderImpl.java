package com.ebank.utility;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class contains methods to read input from console. This class provides
 * implementation for UserInputReader interface methods
 * 
 * @author Team-E
 *
 */
public class UserInputReaderImpl implements UserInputReader {

	Scanner scanner;

	/**
	 * Default constructor of UserInputReaderImpl which initializes Scanner
	 */
	public UserInputReaderImpl() {
		super();
		// Initializing scanner
		this.scanner = new Scanner(System.in);
	}

	/**
	 * This is the implementation of UserInputReader readInt() method. This method
	 * reads int type value from console
	 */
	@Override
	public int readInt() {
		int intValue = 0;
		try {
			// reading input from console
			intValue = scanner.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("please enter integer type only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return intValue;
	} // End of readInt

	/**
	 * This is the implementation of UserInputReader readDouble() method. This
	 * method reads double type value from console
	 */
	@Override
	public double readDouble() {
		double doubleValue = 0.0d;
		try {
			// reading input from console
			doubleValue = scanner.nextDouble();
		} catch (InputMismatchException e) {
			System.out.println("please enter double type only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return doubleValue;
	} // End of readDouble

	/**
	 * This is the implementation of UserInputReader readString() method. This
	 * method reads String type value from console
	 */
	@Override
	public String readString() {
		String stringValue = null;
		try {
			// reading input from console
			stringValue = scanner.next();
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return stringValue;
	} // End of readString

	/**
	 * This is the implementation of UserInputReader readStringLine() method. This
	 * method reads String type value from console
	 */
	@Override
	public String readStringLine() {
		String stringValue = null;
		try {
			// reading input from console
			stringValue = scanner.nextLine();
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return stringValue;
	} // End of readStringLine

	/**
	 * This is the implementation of UserInputReader readByte() method. This method
	 * reads byte type value from console
	 */
	@Override
	public byte readByte() {
		byte byteValue = 0;
		try {
			// reading input from console
			byteValue = scanner.nextByte();
		} catch (InputMismatchException e) {
			System.out.println("please enter byte type (i.e. -128 to 127) input only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return byteValue;
	} // End of readByte

	/**
	 * This is the implementation of UserInputReader readShort() method. This method
	 * reads short type value from console
	 */
	@Override
	public short readShort() {
		short shortValue = 0;
		try {
			// reading input from console
			shortValue = scanner.nextShort();
		} catch (InputMismatchException e) {
			System.out.println("please enter short type (i.e. -32.768 to 32,767 ) input only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return shortValue;
	} // End of readShort

	/**
	 * This is the implementation of UserInputReader readLong() method. This method
	 * reads long type value from console
	 */
	@Override
	public long readLong() {
		long longValue = 0L;
		try {
			// reading input from console
			longValue = scanner.nextLong();
		} catch (InputMismatchException e) {
			System.out.println("please enter long type input only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return longValue;
	} // End of readLong

	/**
	 * This is the implementation of UserInputReader readFloat() method. This method
	 * reads float type value from console
	 */
	@Override
	public float readFloat() {
		float floatValue = 0.0f;
		try {
			// reading input from console
			floatValue = scanner.nextFloat();
		} catch (InputMismatchException e) {
			System.out.println("please enter float type input only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return floatValue;
	} // End of readFloat

	/**
	 * This is the implementation of UserInputReader readChar() method. This method
	 * reads char type value from console
	 */
	@Override
	public char readChar() {
		char charValue = '\u0000';
		try {
			// reading input from console
			charValue = scanner.next().charAt(0);
		} catch (InputMismatchException e) {
			System.out.println("please enter char type input only");
		} catch (IllegalStateException e) {
			System.out.println("stream is not available to read input from console");
		} catch (Exception e) {
			System.out.println("error in reading input");
		}
		return charValue;
	} // End of readChar

}
