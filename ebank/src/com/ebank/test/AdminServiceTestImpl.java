package com.ebank.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ebank.beans.AdminBean;
import com.ebank.service.AdminService;
import com.ebank.service.AdminServiceImpl;

public class AdminServiceTestImpl {
AdminService service = null;
	
	@Before
	public void setUp() {
		System.out.println("set up");
		service = new AdminServiceImpl();
		
	}
	
	@After
	public void tearDown() {
		System.out.println("tear down");
		service = null;
		
	}
	/**
	 * This method is used to register the new admin test in positive case
	 */
	@Test
	public void doRegistrationTestPositive() {

		AdminBean admin = new AdminBean();
		admin.setAdminId(3005);
		admin.setAdminName("icici");
		admin.setPassword("Icici@123");
		admin.setGender("M");
		try {
			admin.setDateOfBirth(new SimpleDateFormat("dd/mm/yyyy").parse("12/12/1999"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		admin.setMailId("icici@gmail.com");
	
		admin.setPhoneNum("7986453120");
		assertTrue(service.doRegistration(admin));
	}
	/**
	 * This method is used to register the new admin test in negative case
	 */
	@Test
	public void doRegistrationTestNegative() {

		AdminBean admin = new AdminBean();
		admin.setAdminId(3005);
		admin.setAdminName("icici");
		admin.setPassword("Icici@123");
		admin.setGender("M");
		try {
			admin.setDateOfBirth(new SimpleDateFormat("dd/mm/yyyy").parse("12/12/1999"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		admin.setMailId("icici@gmail.com");
	
		admin.setPhoneNum("7986453120");
		assertFalse(service.doRegistration(admin));
	}
	
	/**
	 * This doLogin method check whether the adminId exists or not
	 */
	@Test
	public void doLoginTestPositive() {
		assertTrue(service.doLogin(2222, "Mohan@123"));
	}
	
	
	/**
	 * This doLogin method check whether the adminId exists or not
	 */
	@Test
	public void doLoginTestNegative() {
		assertFalse(service.doLogin(2222, "Mohan@123"));
	}
	/**
	 * This method test whether the given id is their or not
	 */
	@Test
	public void getAdminByIdTestPositive() {
		assertNotNull(service.getAdminById(2222));
	}
	/**
	 * This method test whether the given id is their or not in negative case
	 */
	@Test
	public void getAdminByIdTestNegative() {
		assertNull(service.getAdminById(2222));
	}
	/**
	 * This method is used to validate the Id and date format tested in Positive case
	 */
	@Test
	public void updateDateTestPositive() {
		try {
			assertTrue(service.updateDate(2222, new SimpleDateFormat("dd/mm/yyyy").parse("21/01/1996")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * This method is used to validate the Id and date format tested in negative case
	 */
	@Test
	public void updateDateTestNegative() {
		try {
			assertFalse(service.updateDate(222, new SimpleDateFormat("dd/mm/yyyy").parse("21/01/1996")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * This method is used to return true if the password is updated successfully
	 */
	@Test
	public void updatePasswordTestPositive() {
	boolean result = service.updatePassword(2222, "Mohan@567");
	assertTrue(result);
	}
	/**
	 * This method is used to return true if the password is not updated successfully
	 */
	@Test
	public void updatePasswordTestNegative() {
	boolean result = service.updatePassword(222, "Mohan@567");
	assertFalse(result);
	}
	/**
	 * This method is used to return true if the name is updated successfully
	 */
	@Test
	public void updateNameTestPositive() {
	boolean result = service.updateName(2222, "Mohan");
	assertTrue(result);
	}
/**
 *  This method is used to return true if the name is not updated successfully
 */
	@Test
	public void updateNameTestNegative() {
	boolean result = service.updateName(222, "Mohan");
	assertFalse(result);
	}
	
}
