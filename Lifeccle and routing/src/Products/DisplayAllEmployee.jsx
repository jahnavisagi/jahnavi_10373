import React, { Component } from 'react';
import './style.css'
class DisplayAllEmployees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
            .then(response => response.json())
            .then(employeeData => {
                this.setState({
                    isLoaded: true,
                    employees: employeeData
                })
            });
    }
    render() {
        var { isLoaded, employees } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div> 
               <center>
                   <h3>Employee Details</h3>
                    <table border="1" style={{padding:"2px",marginTop:"3%",borderCollapse: "collapse"}}>
                    <col width="180"/>
                    <col width="180"/>
                    <col width="180"/>
                    <col width="180"/>
                    <col width="180"/>
        
                        <tr><th >ID</th>  
                        <th> Name  </th>
                        <th>  Role </th>
                        <th>   Password </th>
                        <th>    Security Question</th></tr>
                        {employees.map(employee => (
                    <tr>
                   
                        <td style={{textAlign:"center" }} key={employee.id}>{employee.employeeId}</td> 
                        <td style={{textAlign:"center" }}  key={employee.id}>{employee.employeeName}</td>
                        <td style={{textAlign:"center" }}  key={employee.id}>{employee.role}</td>
                        <td style={{textAlign:"center" }}  key={employee.id}>{employee.employeePassword}</td>
                        <td style={{textAlign:"center" }}  key={employee.id}>{employee.securityQuestion}</td>

                    </tr>
                   ))}
                   </table>
                   </center>
            </div>
        );
    }
}
export default DisplayAllEmployees;