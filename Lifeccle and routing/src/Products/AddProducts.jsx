
import React, { Component } from 'react';


class AddProducts extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname : '',
            price : '',
            quantity :'',
            productType : ''
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("name"+this.state.productname);
        console.log("price"+this.state.price);
        console.log("quantity"+this.state.quantity);
        console.log("producttype"+this.state.productType);
        const data = {
            'productname':this.state.productname,
            'price':this.state.price,
            'quantity':this.state.quantity,
            'productType':this.state.productType
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/addProducts",{
            method : 'POST',
            // mode:'no-cors',
            // headers :{'content-Type' : 'application/json;charset=utf-8'},
            headers:{   "Accept": "application/json",'Content-Type' : 'application/json;charset=utf-8'},
            body : JSON.stringify(data)
            
        })
        .then((res) => res.json())
        .then((data) => alert('added successfully'))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" name="productname" placeholder="Enter Product Name" value={this.state.productname} onChange={this.handleChange.bind(this)}/><br/>
                    <input type="text" name="price" placeholder="Enter Product Price" value={this.state.price} onChange={this.handleChange.bind(this)}/><br/>
                    <input type="text" name="quantity" placeholder="Enter Product Quantity" value={this.state.quantity}  onChange={this.handleChange.bind(this)}/><br/>
                    <input type="text" name="productType" placeholder="Enter Product Type" value={this.state.productType} onChange={this.handleChange.bind(this)}/><br/>
                    <button>ADD PRODUCT</button>
                </form>
            </div>
         );
    }
}
 
export default AddProducts;
