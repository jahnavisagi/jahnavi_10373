import React, { Component } from 'react';
import './style.css'

class DisplayProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({
                    isLoaded: true,
                    items: data
                })
            });
    }
    render() {
        var { isLoaded, items } = this.state;
        if (!isLoaded) {
            return <div>Loading Products...</div>;
        }
        return (
            <div className="App">
              <center>
              <h4>Product Details</h4>
                <table border="1" style={{padding:"2px",marginTop:"3%",borderCollapse: "collapse"}}>
                <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="100"/>
                     <tr>     
                       <th>Product Id</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>ProductType</th>
                    </tr>
       
                    {items.map(item => (
                        <tr>
                            <td style={{textAlign:"center"}} key={item.product_id}>{item.product_id}</td>
                            <td style={{textAlign:"center"}} key={item.product_id}>{item.productname}</td>
                            <td style={{textAlign:"center"}} key={item.product_id}>{item.price}</td>
                            <td style={{textAlign:"center"}} key={item.product_id}>{item.quantity}</td>
                            <td style={{textAlign:"center"}} key={item.product_id}>{item.productType}</td>
                        </tr>
                    ))}
                </table>
                </center>
            </div>
        );
    }
}
export default DisplayProducts;