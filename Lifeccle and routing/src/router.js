import React from 'react';
import Clock from './components/clock';
import Register from './components/register';
import Home from './Home';
import {BrowserRouter} from 'react-router-dom';
import {Switch,Route} from 'react-router-dom';
import Employee from './components/employees';
import EmployeeDetail from './components/employeedetails';
import Main from './Products/main';
import DisplayProducts from './Products/DisplayProducts';
import DisplayEmployees from './Products/DisplayAllEmployee';
import AddProducts from './Products/AddProducts';

const Routes = () => (
    <BrowserRouter>
    <Switch>
    {/* <Route exact path={`/`} component ={Home}/>
    <Route path={`/clock`} component ={Clock}/>
    <Route path={`/register`} component ={Register}/>
    <Route path={`/employees`} component={Employee} exact/>
    <Route path="/employee/:id" component={EmployeeDetail} exact/> */}
<Route exact path={`/`} component ={Main}/>
<Route path={`/DisplayAllEmployees`} component ={DisplayEmployees}/> 
<Route path={`/DisplayProducts`} component ={DisplayProducts}/> 
<Route path={`/AddProducts`} component ={AddProducts}/>

    </Switch>
    </BrowserRouter>
);

export default Routes;