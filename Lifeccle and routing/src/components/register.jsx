import React,{Component} from 'react';

class Register extends Component{
    constructor(props) {
        super(props);
        this.state={
            username:'',
            password:'',
            fav: ''
        }
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
         [event.target.name] : event.target.value
        });
      }

      handleSubmit(event) {
        alert('A name was submitted: ' + this.state.username + 'FAV:' + this.state.fav);
        event.preventDefault();
      }

    render(){
        return (
            <div>
                <h3>Regitration form</h3>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        UserName:
                            <input type="text" name="username" value={this.state.username} onChange={this.handleChange}/>
                    </label>  
                   <br/> <label>
                        Password:
                            <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/>
                    </label> <br/>
                    <label>
          Pick your favorite flavor:
          <select name="fav" value={this.state.fav} onChange={this.handleChange}>
            <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option>
          </select>
        </label> 
                   <br/> <input type="submit" value="Submit"/>
                </form>    
            </div>
        );
    }
}
 

export default Register;