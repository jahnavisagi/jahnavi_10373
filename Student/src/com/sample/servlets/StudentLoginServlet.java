package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentLoginServlet
 */
@WebServlet("/StudentLoginServlet")
public class StudentLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public StudentLoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

    /**
	 * This method is used to send response to client in the form html page by
	 * taking the request from the client in html format
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int userId = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("pwd");
		response.setContentType("text/html");
		Student student = new Student();
		StudentService studentService = new StudentServiceImpl();
		student = studentService.findById(userId);
		RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");
		//this checks the username and password. if both matches then the user will be logged in
		if (userId==student.getId() && password.equals(student.getPassword())) {
			request.setAttribute("student", student);
			rd.forward(request, response);
		} else {
			response.sendRedirect("errorlogin.html");
		}
	}

}
