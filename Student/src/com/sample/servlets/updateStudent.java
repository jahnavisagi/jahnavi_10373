package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class updateStudent
 */
@WebServlet("/updateStudent")
public class updateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }


    /**
	 * This method is used to send response to client in the form html page by
	 * taking the request from the client in html format
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
		String password=request.getParameter("password");
		
		//creating the reference object for studentbean
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		student.setPassword(password);
		
		
		StudentService service = new StudentServiceImpl();
		//by using the reference object we can call update method in service class
		boolean result = service.updateStudent(student);
				
		PrintWriter out = response.getWriter();
		
		out.print("<html><body>");
		
		//if record is updated then this if block will get executed.
		if(result ) {
			out.print("<h2>Student updated</h2>");
			out.println("<table border='1'>");
	   		out.println("<tr><th>ID</th><th>Name</th><th>Age</th><th>Password</th></tr>");
	   		
	   			
	   			out.println("<tr><td>"+student.getId()+"</td>");
	   			out.println("<td>"+student.getName()+"</td>");
	   			out.println("<td>"+student.getAge()+"</td>");
	   			out.println("<td>"+student.getPassword()+"</td></tr>");
	   		
	   		out.println("</table>");
		}
		else {
			out.print("<h2>Something wrong</h2>");
		}
		
		out.print("</body></html>");
		//closing the printwriter class
		out.close();
		
		
		
		
	}

}
