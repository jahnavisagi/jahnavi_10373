<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Records</title>
</head>
<body>
<table border="1">
<tr><th>Id</th>
<th>Name</th>
<th>Age</th>
<th>Password</th>
<th>City</th>
<th>State</th>
<th>Pincode</th></tr>
<c:forEach var ="student" items="${studentList}">
<tr><td>${student.id }</td>
<td>${student.name }</td>
<td>${student.age }</td>
<td>${student.password }</td>
<td>${student.address.city }</td>
<td>${student.address.state }</td>
<td>${student.address.pincode }</td></tr>
</c:forEach>
</table>
</body>
</html>