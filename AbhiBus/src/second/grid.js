import React, { Component } from 'react';
import './grid.css'
import Grid1 from'../grid1.jpg';
import HomeBanner4 from '../homepagebanner(4).jpg';
import HomeBanner from '../homepagebanner-hotels.jpg';
import Hyderabad from '../hyderabad.jpg';
import Footer1 from './footer1';
import Banglore from '../Banglore.png';
import Mumbai from '../Mumbai.PNG';

class Grid extends Component {
    render() {
        
      return (
          <div>
        <div style={{width:"80%",marginLeft:"10%",marginTop:"10%"}}>
             <div className="row">
                <div className="col-sm-3">
                   <img src={Grid1} alt="offers" style={{width:"350px",height:"170px"}}/>
                
                </div>
                <div className="col-sm-1"></div>
                <div className="col-sm-3">
                <img src={HomeBanner4} alt="offers" style={{width:"350px",height:"170px"}}/>
                
                </div><div className="col-sm-1"></div>
                <div className="col-sm-3">
                <img src={HomeBanner} alt="offers" style={{width:"350px",height:"170px"}}/>
                
                </div>
             </div>
        </div>

        <div style={{width:"80%",marginLeft:"18%",marginTop:"2%"}}>
             <div className="row">
                <div className="col-sm-3">
                <div className="card" style={{width: "18rem"}}>
                <img src={Hyderabad} style={{marginLeft:"-110px",width:"350px",height:"170px"}} className="card-img-top" alt="offers"/>
  <div className="card-body">
    <div style={{marginLeft:"-110px"}}>
    <p className="card-text">Price Starts at<br/></p>
    <p style={{color:"red"}}><i className="fa fa-rupee"></i>469</p>
    </div>
    <a href="#booknow" className="btn btn-danger" style={{marginLeft:"130px",marginTop:"-45%"}}>Book Now</a>
  </div>
</div>
                  
                
                </div>
                <div className="col-sm-1"></div>
                <div className="col-sm-3">
                <div className="card" style={{width: "18rem"}}>
                <img src={Banglore} style={{marginLeft:"-110px",width:"350px",height:"170px"}} className="card-img-top" alt="offers"/>
  <div className="card-body">
    <div style={{marginLeft:"-110px"}}>
    <p className="card-text">Price Starts at<br/></p>
    <p style={{color:"red"}}><i className="fa fa-rupee"></i>440</p>
    </div>
    <a href="#submit" className="btn btn-danger" style={{marginLeft:"130px",marginTop:"-45%"}}>Book Now</a>
  </div>
</div>
                  
                
                </div>
                <div className="col-sm-1"></div>
                <div className="col-sm-3">
                <div className="card" style={{width: "18rem"}}>
                <img src={Mumbai} style={{marginLeft:"-110px",width:"350px",height:"170px"}} className="card-img-top" alt="offers"/>
  <div className="card-body">
    <div style={{marginLeft:"-110px"}}>
    <p className="card-text">Price Starts at<br/></p>
    <p style={{color:"red"}}><i className="fa fa-rupee"></i>449</p>
    </div>
    <a href="#book" className="btn btn-danger" style={{marginLeft:"130px",marginTop:"-45%"}}>Book Now</a>
  </div>
</div>
                  
                
                </div>
                <div className="col-sm-1"></div>
             </div>
        </div>
        <div className="abhibus-info" >
<h4>Why book with Abhibus?</h4><br/>
<p>Finding a hotel for your vacation or a business trip just got easier with Abhibus- the fastest growing market place for online bus tickets. Abhibus has curated hotel content understanding the staying needs of our bus travellers. We realized what was the point in publishing all the inventory available which may be of no relevance to our customers at all . This very thought triggered us to curate the content and enter into strategic alliances with leading three star, two star hotels and hotel aggregators from across the country who could give the quality and price without compromise.</p> <p>We are sure you will find the accommodation of your choice within budget at Abhibus.</p> 

</div>
<br />
<div style={{align:"center"}}>
<p style={{textAlign:"center"}}>We're Social! Connect with us on-
<a href="#FB">Facebook</a><span> , </span>
<a href="#twitter">Twitter</a><span> , </span>
<a href="#google">Google+</a><span> , </span>
<a href="#youtube">Youtube</a>
</p>
</div>
       
<Footer1 />
        </div>
      );
    }
}
export default Grid;