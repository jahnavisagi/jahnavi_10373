import React, { Component } from 'react';
import '../Components/navbar.css';
import Logo from '../logo1.png';
import Content1 from './content1';


class Navbar extends Component {
    render() {
        
      return (
          <div>
        <div className="navbar">
        
         <div className="dropdown">
         
          <button className="dropbtn">
          <i className="fa fa-phone" style={{fontSize:"15px",margi:"5px"}}></i>
          Customer Care - 1860 108 6789
            <i className="fa fa-caret-down" style={{fontSize:"15px",marginLeft:"5px"}}></i>
          </button>
          <div className="dropdown-content">
            <a href="#banglore">Bengalore - (080) 33 66 77 99 </a><hr/>
            <a href="#chennai">Chennai - (044) 33 66 77 99</a><hr/>
            <a href="#delhi">Delhi - 1860 200 9999</a><hr/>
            <a href="#hyderabad">Hyderabad - (040) 61 65 67 89 </a><hr/>
            <a href="#kolkata">Kolkata - 1860 200 9999</a><hr/>
            <a href="#mumbai">Mumbai - 1860 200 9999</a><hr/>
            <a href="#pune">Pune - 1860 200 9999 </a>
            <a href="#vijayawada">Vijayawada - (040) 61 65 67 89 </a>
            <a href="#customersupport">Customer Support - 1860 200 9999  </a>

          </div>
        </div> 
        <a href="#news">Cancel Booking</a>
         <a href="#news">Print Booking</a>
         <a href="#news">Offers</a>
        <a href="#home" className="active">Hotels</a>
        <a href="#home">Bus</a>
        <div style={{marginTop:"30px"}}  className="topnav-left">
        <i className="fa fa-user-circle-o" style={{marginTop:"12%"}}></i>
       <a href="#login/signup">Signup/Login</a>
       </div>
      </div>
    
<div className="bgimg">
<br/>
<center style={{marginTop:"30px"}}><img src={Logo} alt="abhibus"/></center>

</div>
<Content1 />
  </div>
      );
    }
  }

  export default Navbar;