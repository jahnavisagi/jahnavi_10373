import React, { Component } from 'react';

import './App.css';
import {Route} from 'react-router-dom';

import NavBar from './second/navbar1';
import Nav from './Components/navbar';


class App extends Component {
  render() {
    return (

      <div>
        <div>
          <Route path="/" exact component={Nav}/>
          <Route path="/hotel" component={NavBar}/>
        </div>
      </div>
    );
  }
}

export default App;
