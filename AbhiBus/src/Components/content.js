import React, { Component } from 'react';
import './content.css'
import Block from './block'
import Banner from '../home-left-banner.png'

class Content extends Component {
    render() {

        return (
            <div >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">


                            <img class="bus" src={Banner} alt="left-banner" />


                        </div>
                        <div class="col-sm-4">
                            <p class="center">Search for bus tickets</p>
                            <form>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="Leaving From" placeholder="Leaving From" /></div></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" name="Going to" placeholder="Going To" /></div></div>
                                        <div class="row">
                                    <div class="col-sm-6">
                                    <input type="date" name="Date Of Journey" placeholder="Date OfJourney" /></div>
                                    <div class="col-sm-6">
                                        <input style={{ marginLeft: "10px" }} type="date" name="Date Of Return" placeholder="ReturnDate-opt" /></div>
                           </div>
                                <button class="button" type="submit" ><i class="fa fa-search" style={{ fontSize: "18px" }}></i>   Search</button>
                                
    
    </form>

                    </div>
                </div>

            </div>
            <Block />
</div >




      
      );
    }
}

export default Content;