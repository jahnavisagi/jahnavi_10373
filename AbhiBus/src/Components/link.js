import React, { Component } from 'react';
import './link.css';
import Footer from './footer';
class Link extends Component{
    render(){
        return(
           
<div  >
   <div className="background1">
    <div className="abhibus-info-new">
    <p id="demo">Watch abhibus TV Campaign-    
	 <a  href="https://www.abhibus.com/videos#telugu">Telugu </a> <span>|</span>
     <a href="https://www.abhibus.com/videos#hindi"> Hindi</a> <span>|</span>
      <a href="https://www.abhibus.com/videos#tamil">Tamil</a> <span>|</span> 
      <a href="https://www.abhibus.com/videos#kannada">Kannada</a> <span>|</span> 
      <a href="https://www.abhibus.com/videos#malayalam">Malayalam</a>
      </p></div>
    </div>

      
   <br/> <br/> <br/>
    <div className="abhibus-info">
        <div className="titles"><div className="col1"><h2>100000</h2><h4>ROUTES</h4></div> 
        <div className="col2"><h2>2500</h2><h4>BUS OPERATORS</h4></div> 
        <div className="col3"><h2>300+</h2><h4>hosted operators</h4></div>
    </div>
        <p>When you book with abhibus.com, you get access to <strong>bus tickets</strong> directly from all the major Bus Operators because we host the inventory of India's largest fleet Operators and we are their technology Partners. Needless to say the customer support is 24X7, spontaneous &amp; happy to help as we have direct access to all Operators.</p>
</div>
  <br/><br/>
<div className="abhibus-info" >
<h3>Book Bus Tickets only at Abhibus</h3><br/>
<p>Only for you we got rid of the long queues on jammed streets and bus stations for <strong>bus tickets</strong>, we got rid of agents cheating you with jacked up bus ticket prices, we are the pioneers who revolutionized the <strong>online bus booking</strong> in India.  Booking a bus ticket from your couch or when you are on the move was made possible by us by just logging on to abhibus.com or booking through the Abhibus app. </p>
<p>You could<strong> book bus tickets</strong> to anywhere in India from over 2500 bus operators covering over 100000 routes for Sleeper, AC, Semi luxury, Luxury, and coaches from Volvo, Scania, Ashok Leyland to Mercedes at click of a button and to make payment convenient we have multiple payment options like, Debit/Credit, Net Banking or Abhicash Wallet. </p>
<p>Abhibus Customer Service representatives are available 24 X7 365 days a year, who are happy to help you book your <strong>bus ticket online</strong> or attend to any of your queries.</p>

<p>So next time you think of bus, just Abhibus it...</p>

</div>

<hr style={{width:"75%",marginLeft:"145px"}}/>
<div style={{align:"center"}}>
<div className="social-links">
<p style={{textAlign:"center"}}>We're Social! Connect with us on-
<a href="#facebook"><i className="fa fa-facebook fa-lg"></i></a>
    <a href="#twitter"><i className="fa fa-twitter fa-lg"></i></a>
    <a href="#googleplus"><i className="fa fa-google-plus fa-lg"></i></a>
    <a href="#youtube"><i className="fa fa-youtube fa-lg"></i></a>
    <a href="#instagram"><i className="fa fa-instagram fa-lg"></i></a>
    <a href="#tumblr"><i className="fa fa-tumblr fa-lg"></i></a>
    <a href="#reddit"><i className="fa fa-reddit fa-lg"></i></a>
    <a href="#pinterest"><i className="fa fa-pinterest fa-lg"></i></a>
    </p>
</div>
    </div>

<Footer />
</div>



        );
    }
}

export default Link;