import React, { Component } from 'react';
import HomeBanner from '../home-page-banner.png';
import HomeBanner1 from '../homepagebanner(1).jpg';
import HomeBanner2 from '../homepagebanner(3).jpg';
import HomeBanner3 from '../homepagebanner(4).jpg';
import HomeBanner4 from '../homepagebanner(5).jpg';
import HomeBanner5 from '../homepagebanner-ksrtc.jpg';
import HomeBanner6 from '../homepagebanner-hotels.jpg';
import HomeBanner7 from '../homepagebanner.jpg';
import "react-responsive-carousel/lib/styles/carousel.min.css";

import './block.css';
import Link from './link';
class Block extends Component{
    render(){
        return(
         
<div>
<div class="container" style={{width:"80%",marginTop:"20%"}}>
                <div class="row">
                    <div class="col-sm-6">
                        <img style={{height: "auto", maxWidth:"475px", maxHeight:"225px",width:"auto"}} src={HomeBanner} alt="home-banner"/>
                     </div>  
                    <div class="col-sm-6" >
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
      <li data-target="#myCarousel" data-slide-to="6"></li>
      <li data-target="#myCarousel" data-slide-to="7"></li>
      
    </ol>

 
    <div class="carousel-inner"  style={{width:"475px"}}>
      <div class="item active">
        <img src={HomeBanner1} alt="Offers" style={{maxWidth:"475px",width:"auto"}}/>
      </div>

      <div class="item">
        <img  src={HomeBanner} alt="Offers" style={{maxWidth:"475px",width:"auto"}}/>
      </div>

      <div class="item">
        <img  src={HomeBanner2} alt="Chicago" style={{maxWidth:"475px",width:"auto"}}/>
      </div>
    
      <div class="item">
        <img  src={HomeBanner3} alt="New york" style={{maxWidth:"475px", width:"auto"}}/>
      </div>

      <div class="item">
        <img  src={HomeBanner4} alt="Offers" style={{maxWidth:"475px",width:"auto"}}/>
      </div>

      <div class="item">
        <img  src={HomeBanner5} alt="Offers" style={{maxWidth:"475px",width:"auto"}}/>
      </div>

      <div class="item">
        <img  src={HomeBanner6} alt="Offers" style={{maxWidth:"475px",width:"auto"}}/>
      </div>

      <div class="item">
        <img  src={HomeBanner7} alt="Offers" style={{maxWidth:"475px",width:"auto"}}/>
      </div>

    </div>

    
  
  </div>
            </div>
                   
                </div>
            </div>
            <br/>
            <Link />

            </div>


        );

    }
}

export default Block;