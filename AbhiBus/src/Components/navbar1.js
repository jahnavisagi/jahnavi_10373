import React, { Component } from 'react';
import './navbar.css'
import Logo from '../logo1.png';
import Content1 from './content1';


class Navbar extends Component {
    render() {
        
      return (
          <div>
        <div class="navbar">
        
         <div class="dropdown">
         
          <button class="dropbtn">
          <i class="fa fa-phone" style={{fontSize:"15px",margi:"5px"}}></i>
          Customer Care - 1860 108 6789
            <i class="fa fa-caret-down" style={{fontSize:"15px",marginLeft:"5px"}}></i>
          </button>
          <div class="dropdown-content">
            <a href="#">Bengalore - (080) 33 66 77 99 </a><hr/>
            <a href="#">Chennai - (044) 33 66 77 99</a><hr/>
            <a href="#">Delhi - 1860 200 9999</a><hr/>
            <a href="#">Hyderabad - (040) 61 65 67 89 </a><hr/>
            <a href="#">Kolkata - 1860 200 9999</a><hr/>
            <a href="#">Mumbai - 1860 200 9999</a><hr/>
            <a href="#">Pune - 1860 200 9999 </a>
            <a href="#">Vijayawada - (040) 61 65 67 89 </a>
            <a href="#">Customer Support - 1860 200 9999  </a>

          </div>
        </div> 
        <a href="#news">Cancel Booking</a>
         <a href="#news">Print Booking</a>
         <a href="#news">Offers</a>
        <a href="#home" class="active">Hotels</a>
        <a href="#home">Bus</a>
        <div style={{marginTop:"30px"}}  class="topnav-left">
        <i class="fa fa-user-circle-o" style={{marginTop:"12%"}}></i>
       <a href="#login/signup">Signup/Login</a>
       </div>
      </div>
    
<div class="bgimg">
<br/>
<img  src={Logo} alt="abhibus" align="middle"/>

</div>
<Content1 />
  </div>
      );
    }
  }

  export default Navbar;