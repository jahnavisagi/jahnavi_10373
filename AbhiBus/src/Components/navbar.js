import React, { Component } from 'react';
import './navbar.css'
import Logo from '../logo1.png'
import Content from './content'
import {Link} from 'react-router-dom'


class Navbar extends Component {
    render() {
        
      return (
          <div>
        <div class="navbar">
        
         <div class="dropdown">
         
          <button class="dropbtn">
          <i class="fa fa-phone" style={{fontSize:"15px",margi:"5px"}}></i>
          Customer Care - 1860 108 6789
            <i class="fa fa-caret-down" style={{fontSize:"15px",marginLeft:"5px"}}></i>
          </button>
          <div class="dropdown-content">
            <a href="#">Bangalore  - 1860 108 6789</a><hr/>
            <a href="#">Delhi  - 1860 108 6789</a><hr/>
            <a href="#">Hyderabad  - (040) 61 65 67 89</a><hr/>
            <a href="#">Mumbai  - 1860 108 6789</a><hr/>
            <a href="#">Pune  - 1860 108 6789</a><hr/>
            <a href="#">Vijayawada  - (040) 61 65 67 89</a><hr/>
            <a href="#">Customer Care - 1860 108 6789</a>
            
          </div>
        </div> 
        <a href="#news">Cancel Ticket</a>
         <a href="#news">Print Ticket</a>
         <a href="#news">Offers</a>
        <Link to="/hotel">Hotels</Link>
        <div style={{marginTop:"30px"}}  class="topnav-left">
        <i class="fa fa-user-circle-o" style={{marginTop:"12%"}}></i>
       <a href="#login/signup">Signup/Login</a>
       </div>
      </div>
    
<div class="background">
<img className="img1" src={Logo} alt="abhibus" align="middle"/>
<Content />
</div>

  </div>
      );
    }
  }
  
  export default Navbar;