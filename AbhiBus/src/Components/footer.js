import React, { Component } from 'react';
import './footer.css';

class Footer extends Component{
    render(){
        return(
<div>
            <div className="mainfooter">
	<div className="wrap clearfix"><br/>
		<div className="col1">
                    <h3 style={{paddingLeft:"15%"}}>Popular Routes </h3>
			    <ul type="none">
    <li><a href="https://www.abhibus.com/buses/1/Hyderabad-Bangalore">Hyderabad - Bangalore </a> </li>
    <li><a href="https://www.abhibus.com/buses/58/Pune-Shirdi">Pune - Shirdi </a> </li>
    <li><a href="https://www.abhibus.com/buses/24/Chennai-Coimbatore">Chennai - Coimbatore </a> </li>
    <li><a href="https://www.abhibus.com/buses/8/Hyderabad-Visakhapatnam">Hyderabad - Visakhapatnam </a> </li>
    <li><a href="https://www.abhibus.com/buses/10/Bangalore-Hyderabad">Bangalore - Hyderabad </a> </li>
    <li><a href="https://www.abhibus.com/buses/17/Bangalore-Vijayawada">Bangalore - Vijayawada </a> </li>
    <li><a href="https://www.abhibus.com/buses/3/Hyderabad-Chennai">Hyderabad - Chennai </a> </li>
  </ul>
		</div>
		<div className="col2">
			     <ul type="none">
    <li><a href="https://www.abhibus.com/buses/108/Delhi-Manali">Delhi - Manali </a> </li>
    <li><a href="https://www.abhibus.com/buses/44/Mumbai-Bangalore">Mumbai - Bangalore </a> </li>
    <li><a href="https://www.abhibus.com/buses/23/Chennai-Bangalore">Chennai - Bangalore </a> </li>
    <li><a href="https://www.abhibus.com/buses/139/Indore-Bhopal">Indore - Bhopal </a> </li>
    <li><a href="https://www.abhibus.com/buses/2/Bangalore-Chennai">Bangalore-Chennai</a> </li>
    <li><a href="https://www.abhibus.com/buses/57/Pune-Mumbai">Pune - Mumbai </a> </li>
    <li><a href="https://www.abhibus.com/buses/67/Ahmedabad-Mumbai">Ahmedabad - Mumbai </a> </li>
  </ul>
		</div>
		<div className="col3">
			 <h3>Popular Bus Operators </h3>
    <a href="https://www.abhibus.com/operator/919/APSRTC">APSRTC </a> <span> | </span>
	<a href="https://www.abhibus.com/operator/923/TSRTC">TSRTC</a> <span> | </span>
	<a href="https://www.abhibus.com/operator/1476/KSRTC">KSRTC</a><span> | </span> 
	<a href="https://www.abhibus.com/operator/781/GSRTC">GSRTC</a> <span> | </span>
	<a href="https://www.abhibus.com/operator/784/HRTC">HRTC</a><span> | </span>
    <a href="https://www.abhibus.com/operator/780/MSRTC">MSRTC</a><span> | </span>
    <a href="https://www.abhibus.com/operator/778/RSRTC">RSRTC</a><span> | </span>
    <a href="https://www.abhibus.com/operator/1486/Kerala-RTC">Kerala RTC</a><span> | </span>
	<a href="https://www.abhibus.com/operator/1487/Pepsu-prtc">PRTC</a><span> | </span>
    <a href="https://www.abhibus.com/operator/20/VRL-Travels">VRL Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/48/SRS-Travels">SRS Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/29/Orange-Travels">Orange Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/36/Morning-Star-Travels">Morning Star Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/24/KPN-Travels">KPN Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/7/Kaveri-Travels">Kaveri Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/3/Kallada-Travels">Kallada Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/8/Sreekaleswari-Travels">Kaleswari Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/19/Neeta-Travels">Neeta Travels </a>   <span> | </span>
    <a href="https://www.abhibus.com/operator/2/SVR-Travels">SVR Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/5/Diwakar-Travels">Diwakar Travels </a><span> | </span>

    <a href="https://www.abhibus.com/operator/25/Paulo-Travels">Paulo Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/57/Meghana-Travels">Meghana Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/17/Royal-Cruiser-Travels">Royal Cruiser Travels </a> <span> | </span>
    <a href="https://www.abhibus.com/operator/76/SRE-Travels">SRE_Travels </a> <span> | </span>
    <a href="https://www.abhibus.com/operator/44/SRM-Travels">SRM Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/646/Rathimeena-Travels">Rathimeena Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/42/Seabird-Travels">Seabird Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/64/JBT-Travels" >Jai Bharat  Travels</a><span> | </span>
    <a href="https://www.abhibus.com/operator/28/Bharathi-Travels">Bharathi </a><span> | </span>
	<a href="https://www.abhibus.com/operator/77/Sri-Krishna-Travels">Sri Krishna Travels</a><span> | </span>
	<a href="https://www.abhibus.com/operator/1243/Transit-BUS">Transit BUS</a><span> | </span>
    <a href="https://www.abhibus.com/operator/774/Parveen-Travels">Parveen Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/34/Gujarat-Travels">Gujarat Travels </a><span> | </span>
    <a href="https://www.abhibus.com/operator/43/Sharma-Transports">Sharma Transport </a><span> | </span>
    <a href="https://www.abhibus.com/operator/89/Reddy-Travels">Reddy Express</a>

			
		</div>
	</div>
    <br/>
    </div>
    
    <div>
    <footer>
  <div className="wrap clearfix"><div className="company">
  <li className="ico logosmall"></li> 
  <a href="#operators">Operators</a><span> | </span>  
  <a href="#routes">Routes</a> <span> | </span> 
  <a href="#-offers">Offers</a><span> | </span> 
  <a href="#mobileapps">Mobile </a> <span> | </span> 
  <a href="#about">About</a>  <span> | </span>
  <a href="#news">News</a> <span> | </span>  
  <a href="#careers">Careers</a><span> | </span>
  <a href="#contact">Contact</a> <span> | </span>
  <a href="#abhibuscommunity" >Abhibus Community</a> <span> | </span>
  <a href="#booking-tips">Booking Tips</a> 

  </div>
 
  <div className="links">
    <a href="#faq">FAQs</a><span> | </span>
    <a href="#terms">Terms</a><span> | </span>
    <a href="#privacy">Privacy</a><span> | </span>
    <a href="#agent-abhibus">Agent Login</a>
  </div></div>

</footer>
</div>
<div className="copyright">
	<div className="wrap clearfix">
	<div className="copy">
	<p>Copyright &copy; AbhiBus Services India Pvt. Ltd. All Rights Reserved.</p>
</div>	
<div className="bank"></div>
<div className="privacy">
	<li className="ico insurance"></li>
		<li className="ico paymentsec"></li>
	<span className="ico paygateway"></span>
	<span className="ico paypal"></span>
</div>
	</div>
</div>

</div>

        );
    }
}

export default Footer;