import React from 'react';
import Clock from './components/clock';
import Register from './components/register';
import { Switch, Route } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import Home from './components/Home';
import Cars from './components/cars';
import CarDetails from './components/carDetails';
import NewsById from './components/newsById';
import News from './components/news';

import Login from './components/Login';
import DashBoard from './components/DashBoard';
import UserBoard from './components/user_in';
import Roles from './components/roles';
import Logout from './components/Logout';
import Allusers from './components/FetchAllUsers'


const Routes = () => (
    <BrowserRouter>

        <Switch>

            <Route path="/" component={Home} exact />
            <Route path="/clock" component={Clock} exact />
            <Route path="/register" component={Register} exact />
            <Route path="/cars" component={Cars} exact />
            <Route path="/allusers" component={Allusers} exact />
            <Route path="/cars/:id" component={CarDetails} exact />
            <Route path="/news" component={News} exact />
            <Route path="/news/:id" component={NewsById} exact />
            <Route path="/login" component={Login} exact />
            <Route path="/admin/dashboard" component={DashBoard} exact />
            <Route path="/userboard" component={UserBoard} exact />
            <Route path="/roles" component={Roles} exact />
            <Route path="/logout" component={Logout} exact />



        </Switch>
    </BrowserRouter>
);

export default Routes;