// npm dependencies
import React, { Component } from 'react' 

//import service file
import service from './service';

export default class SearchArticle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchString: "",
            article: []
        };
        this.handleChange = this.handleChange.bind(this);
    }


    componentDidMount() {
        this.setState({
            article: service
        });
        this.refs.search.focus();
    }
    handleChange() {
        this.setState({
            searchString: this.refs.search.value
        });
    }
    render() {
        let SearchArticle = this.state.article;
        let search = this.state.searchString.trim().toLowerCase();

        if (search.length > 0) {
            SearchArticle = SearchArticle.filter(function (Article) {
                return Article.title.toLowerCase().match(search);
            });
        }
 
    
    return(
    <div>
    <h3>React - simple search</h3>
    <div>
        <input
            type="text"
            value={this.state.searchString}
            ref="search"
            onChange={this.handleChange}
            placeholder="type name here"
        />
        <ul>
            {SearchArticle.map(l => {
                return (
                    <li>
                        {l.title} <a href="#">{l.name}</a>
                    </li>
                );
            })}
        </ul>
    </div>
        </div >
      );
    }
  }

