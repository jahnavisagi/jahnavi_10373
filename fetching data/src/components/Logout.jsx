import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';


class LOGOUT extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valid: false
        }
    }
    logOut = e => {
          
        localStorage.removeItem('jwt-token');
        this.setState({ valid: true })
        window.location.reload();
      
    }

    render() {

        if (this.state.valid == true) {
            return (
                <Redirect to="/login" />
            )
        }

        return (
            <button onClick={this.logOut}>LOGOUT</button>
        );
    }
}
export default LOGOUT;