import React, { Component } from 'react';
import './form.css';

class DeleteEmployee extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            employeeId : '',
            role : ''
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("Employee Id: "+this.state.employeeId);
        console.log("role: "+this.state.role);
        const data = {
            employeeId:this.state.employeeId,
            role : this.state.role
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/delEmployee",{
            method : 'DELETE',
            headers :{'content-Type' : 'application/json'},
            body : JSON.stringify(data)
        })
        .then((res) => res.json)
        .then((data) => alert ("Deleted Successfully"))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>  
                <form onSubmit={this.handleSubmit}>
                    Enter Employee id: <br/>
                    <input type="text" name="employeeId" value ={this.state.employeeId} placeholder="Enter Employee Id" onChange={this.handleChange}/><br/>
                    Enter Employee Role: <br/>
                    <input type="text" name="role" value ={this.state.role} placeholder="Enter the Employee Role" onChange={this.handleChange}/><br/>
                    <input type="submit" value= "submit"/>
                </form>
            </div>
         );
    }
}
 
export default DeleteEmployee;