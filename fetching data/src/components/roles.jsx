import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import * as roleActions from '../store/actions/roleActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class UserRoles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userItems: {}
        }
    }
    componentDidMount() {
        this.props.roleActions.fetchUser();
    }

    render() {
        const roles = this.props.userItems;
        return (
            <div>
                {
                    (roles)
                        ?
                        (function () {
                            switch (roles.role) {
                                case 'user':
                                    return <Redirect to='/userboard' />
                                case 'admin':
                                    return <h1>Welcome admin</h1>
                                case 'employer':
                                    return <Redirect to='/admin/dashboard' />

                            }
                        })()
                        :
                        <div>
                            <h1>Loading</h1>
                        </div>
                }
            </div>
        );
    }
}
function mapStateToProps(state) {

    return {
        userItems: state.RoleReducer.myDetails
    }

}

function mapDispatchToProps(dispatch) {
    return {
        roleActions: bindActionCreators(roleActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserRoles);