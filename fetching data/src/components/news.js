import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';

class News extends Component {
    componentDidMount() {

        this.props.newsActions.fetchNews();

    }
    render() {

        return (
            <div>
                <div className="container">
                    <div className="card">
                        <div className="card-body"> <h1>Information about Cancer</h1>
                            <p className="page-desc-text bold-text">Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                        </div>
                    </div>

                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-sm-12 newsmenu">
                            <a className="collection-item active" href="">News</a>
                            <a className="collection-item " href="">NGOs/NPOs</a>
                            <a className="collection-item " href="">Support groups</a>
                            <a className="collection-item" href="">Online help</a>
                        </div> 


                        <div className="col-lg-9 col-sm-12">
                            <div className="row showdiv">  
                                    <p className="newsterm">News</p>
                                    <p className="showall"><a>Show all</a> </p> 
                            </div>
                            <div className="row">
                                {
                                    (this.props.newsItems) ?
                                        <div>
                                            {this.props.newsItems.map(item =>
                                                (<div key={item._id}>
                                                    <h2><a href={`/news/${item._id}`}>{item.heading}</a></h2>

                                                    <h4>{item.brief}</h4>
                                                </div>
                                                ))}
                                        </div>
                                        : <div>
                                            <h2>Loading...</h2>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        );
    }

}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news
    };

}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(News);