import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';

class NewsById extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectednews: {}
        }

    }
    componentWillMount() {
        this.props.newsActions.fetchNewsById(this.props.match.params.id);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.newsItems) {
            this.setState({
                selectednews: newProps.newsItems,
            })
        }
    }


    render() {

        return (
            <div>      

                <div>
                
                    publishDate:{this.state.selectednews.timestamp}
                </div><br /> 
                
                <div>
                    {this.state.selectednews.body}
                </div>
             </div>
        );
    }

}
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.item
    };

}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsById);