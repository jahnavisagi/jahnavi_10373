import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authAction'
import { Redirect } from 'react-router-dom'

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            isSubmitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);

    }


    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }
    doLogin = e => { 
        this.props.authActions.doLogin({
            email: this.state.email,
            password: this.state.password

        })
        this.setState({
            isSubmitted: true
        })

    }

    render() {

        if (this.state.isSubmitted) {
            if  (this.props.isLoggedIn) { 
                window.location.reload();
                return <Redirect to='/roles' />

            }
            else if (this.props.statuscode == -1){
                alert ("Email is not registered with us")
                this.setState ({isSubmitted : false})
            }
            else if (this.props.statuscode == -2){
                alert ("Incorrect password format")
                this.setState ({isSubmitted : false})

            }
            else if (this.props.statuscode == -3){
                alert ("Incorrect password ")
                this.setState ({isSubmitted : false})

            }
        }

        return (
            <div className="form">

                <label>Email: </label>
                <input type="text" id="email" name="email" value={this.state.email} onChange={this.handleChange} placeholder="Enter email"></input><br /><br />

                <label>password:</label>
                <input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="Enter password"></input><br />

                <input onClick={this.doLogin} type="submit" value="submit" />



            </div>
        )
    }
}
function mapStateToProps(state) {

    return {
        isLoggedIn: state.authReducer.is_logged_in,
        statuscode : state.authReducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

