import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import newsService from '../middlewares/newsService';
import AuthService from '../middlewares/authService';
import RoleService from '../middlewares/roleService'; 

export default function configureStore() {
    return createStore (
        rootReducer,

        compose(applyMiddleware(
            newsService,AuthService,RoleService
        ))
    );
};