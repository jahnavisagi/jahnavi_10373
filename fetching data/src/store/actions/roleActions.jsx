import * as allAction from './actionConstants';

export function receiveUser(data) {
    return {
        type: allAction.RECEIVE_USERS, payload: data
    };
}

export function fetchUser() {
    return {
        type: allAction.FETCH_USERS, payload: {}
    };
}