export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
/**
 * 
 * Action Constants for news scenarios
 */
export const FETCH_NEWS = "FETCH_NEWS";
export const RECEIVE_NEWS = "RECEIVE_NEWS";
/**
 * 
 * Action Constants for get news by id
 */

export const FETCH_NEWS_BY_ID = "FETCH_NEWS_BY_ID";
export const RECEIVE_NEWS_BY_ID = "RECEIVE_NEWS_BY_ID";



/**
 * 
 * Action Constants for login 
 */ 
export const Do_LOGINUSER = "Do_LOGINUSER";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS"; 
export const LOGIN_USER_DATA_ERROR = "LOGIN_USER_DATA_ERROR"
export const LOGOUT = "LOGOUT";

/**
 * 
 * Action Constants for get the users 
 */

 export const FETCH_USERS = "FETCH_USERS";
 export const RECEIVE_USERS  = "RECEIVE_USERS";
 