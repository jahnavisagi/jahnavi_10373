import * as allActions from './actionConstants';

export function receiveNews(data) {
    return {
        type: allActions.RECEIVE_NEWS, payload: data
    };
}

export function fetchNews() {
    return {
        type: allActions.FETCH_NEWS, payload: {}
    };
}

export function receiveNewsById(data) {
    return {
        type: allActions.RECEIVE_NEWS_BY_ID, payload: data
    };
}

export function fetchNewsById(id) {
    return {
        type: allActions.FETCH_NEWS_BY_ID, payload: { id }
    };
}