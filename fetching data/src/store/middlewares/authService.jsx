import request from 'superagent';
import * as newsActions from '../actions/authAction';
import * as allActions from '../actions/actionConstants';

const authService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.Do_LOGINUSER:
            request.post("http://13.250.235.137:8050/api/auth/login",action.payload)
                .then(res => {
                    const data = JSON.parse(res.text);
                    if(data.statuscode==1){
                        next({
                            type:allActions.LOGIN_USER_SUCCESS,
                            payload:data.statuscode
                        })
                        localStorage.setItem('jwt-token',res.body.token);
                        
                    }
                  else{
                   
                      next({
                        type: allActions.LOGIN_USER_DATA_ERROR,
                        payload:data.statuscode
                      });
                  }
                })
                 
                .catch(err => {
                    
                    next({ type: 'LOGIN_USER-DATA_ERROR', err });
                });
            break;

        default:
            break;
    }
}
export default authService;