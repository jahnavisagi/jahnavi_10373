import request from 'superagent';
import * as roleActions from '../actions/roleActions';
import * as allActions from '../actions/actionConstants'; 
 
const roleService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_USERS:
        request.get ("http://13.250.235.137:8050/api/user")
        .set('Accept', 'application/json')
        .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
        .then (res =>{
            const data = JSON.parse (res.text);
            next(roleActions.receiveUser(data));
           
        })  
        break; 

        default:
            break;
    }
}


export default roleService;

 