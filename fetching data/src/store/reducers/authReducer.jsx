import * as allActions from '../actions/actionConstants'

const intialState = {
    is_logged_in: false,
   statuscode : 0
}

export default function authReducer(state = intialState, action) {
    switch (action.type) {

        case allActions.LOGIN_USER_SUCCESS: 
            return {
                ...state,
                is_logged_in: true
            };
        case allActions.LOGIN_USER_DATA_ERROR :
            return {
                is_logged_in: false,
                statuscode : action.payload
            }

        default:
            return state;
    }
}