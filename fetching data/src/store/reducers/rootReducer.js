import { combineReducers } from 'redux';
import counterReducer from '../reducers/conterReducer';
import newsReducer from '../reducers/newsReducer';
import authReducer from '../reducers/authReducer'
import RoleReducer from '../reducers/roleReducer'


const rootReducer = combineReducers({
    counterReducer,
    newsReducer,authReducer,RoleReducer
});

export default rootReducer;