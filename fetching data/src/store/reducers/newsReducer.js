import * as allActions from '../actions/actionConstants'

const intialState = {
    news: [],
    item: {},
    isLoaded: false
}

export default function newsReducer(state = intialState, action) {
    switch (action.type) {

        case allActions.FETCH_NEWS:

            return action;

        case allActions.RECEIVE_NEWS:

            return {
                ...state,
                news: action.payload.articles,
                isLoaded: true
            };
        case allActions.FETCH_NEWS_BY_ID:
            return action;

        case allActions.RECEIVE_NEWS_BY_ID:
            return {
                ...state,
                item: action.payload.article,
                isLoaded: true
            };

        default:
            return state;
    }
}