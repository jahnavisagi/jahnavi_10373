import * as allActions from '../actions/actionConstants'

const intialState = {
   
    isLoaded: false,
    myDetails : {}
} 

export default function roleReducer(state = intialState, action) {
    switch (action.type) {

        case allActions.FETCH_USERS:

            return action;

        case allActions.RECEIVE_USERS:

            return {
                ...state,
                myDetails : action.payload.user,
                isLoaded : true
            }; 
        default:
            return state;
    }
}