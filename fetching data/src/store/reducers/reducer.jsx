import * as allActions from '../actions/actionConstants'

const intialState = {
    is_Logged_in:false,
}

export default function authReducer(state = intialState, action) {
    switch (action.type) {
 
        case allActions.LOGIN_USER_SUCCESS:

            return {
                ...state, 
                is_Logged_in: true
            };
        
        default:
            return state;
    }
}