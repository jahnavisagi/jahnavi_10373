import React, { Component } from 'react';
import './App.css'; 
import Routes from './routes'; 
import Logout from './components/Logout'
 
 
 
class App extends Component {
  render() {

    return (
     
      <div>  
      
         <Routes />
      </div>
      
    
    );
  }
}

export default App;
