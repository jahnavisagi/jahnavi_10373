import React, {Component} from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
import {Redirect} from 'react-router-dom';
import './login.css'
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password: '',
            isSubmitted: false

        };
         this.handleChange =this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }

    handleChange = e=> {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }

    doLogin = e => {
        this.props.authActions.doLoginUser({
            email: this.state.email,
            password: this.state.password
        })
        this.setState({
            isSubmitted : true
        })
    }
    render() {
        if(this.state.isSubmitted) {
            if(localStorage.getItem('jwt-token')&& this.props.isLoggedIn===true) {
                return <Redirect to = '/admin/dashboard'/>
            }
            
        }
        return( 
            <div className="top">
                <center>
                <form onSubmit={this.doLogin} method="post">
                <fieldset>
                    <legend>Login</legend>
                    <table>
                     <tr >
                         <th  className="left">Email:</th>
                         <td><input type="text" name="email" placeholder="Enter email" value={this.state.email} onChange={this.handleChange}/>
                         </td>
                    </tr> 
                  <tr ><th  className="left">
                        Password:</th><td><input type="password" name="password" placeholder="Enter password" value={this.state.password} onChange={this.handleChange}/>
                        </td> </tr>
                  <tr>
                  <th></th><td> <input type="submit" value="Submit"/></td>    
                  </tr> 
                </table>
                </fieldset>

                </form>
                </center>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        isLoggedIn: state.is_logged_in,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);

