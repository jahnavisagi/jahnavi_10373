import React, { Component } from 'react';

// Module import
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//Actions import
import * as newsActions from '../store/actions/newsActions';

import './news.css';
class NewsDetails extends Component {

    constructor(props) {
        super(props);
        this.state= {
            newsItem:{}
        }
    }
    componentWillMount() {
        this.props.newsActions.fetchNewsById(this.props.match.params.id);
    }

    componentWillReceiveProps(newsProps) {
        if(newsProps.newsItem) {
            this.setState({
                newsItem: newsProps.newsItem,
            })
        }
    }


    render() {
        return (
            <React.Fragment>
               
                <div>
                publishDate={this.state.newsItem.timestamp}
                </div>

                <div>
                {this.state.newsItem.body}
                </div>
               
            </React.Fragment>
        );
    }

}
function mapStateToProps(state) {
    return {
        newsItem: state.newsItem
    };
}
function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsDetails);