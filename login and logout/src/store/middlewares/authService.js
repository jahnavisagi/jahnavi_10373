import request from 'superagent';
import * as allActions from '../actions/actionConstants';

const authService = store => next => action => {
    next(action)
    console.log('auth service');
    switch (action.type) {
   
        case allActions.DO_LOGIN_USER:
            request.post("http://13.250.235.137:8050/api/auth/login", action.payload)
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statuscode == 1) {
                        console.log('auth service success')
                        next({
                            type: allActions.LOGIN_USER_SUCCESS,
                            payload: data.statuscode
                        })
                        localStorage.setItem('jwt-token', res.body.token);
                        console.log("tokenestablished");
                    } else { 
                        console.log('error in else')
                        next({
                            type: 'LOGIN_USER_DATA_ERROR',
                            payload: data.statuscode
                        });
                    }
                })
                .catch(err => {
                    console.log("service failure");
                    return next({ type: 'LOGIN_USER_DATA_ERROR', err });
                });

            break;


        default:
            break;
    }
}
export default authService;