import * as allActions from '../actions/actionConstants';


export  function receiveNews(data) {
    console.log("receive actions");
    console.log(data);
    return {
        type: allActions.RECEIVE_NEWS, payload: data
    }
}

export  function fetchNews(data) {
    console.log("new service");
    return {
        type: allActions.FETCH_NEWS, payload: {}
    }
}


export  function receiveNewsById(data) {
    console.log("receive actions");
    console.log(data);
    return {
        type: allActions.RECEIVE_NEWS_BY_ID, payload: data
    };
}

export  function fetchNewsById( id ) {
    console.log("new service");
    return {
        type: allActions.FETCH_NEWS_BY_ID, payload: { id }
    };
}