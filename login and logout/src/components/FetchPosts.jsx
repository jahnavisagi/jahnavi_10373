import React, { Component } from 'react';

class FetchPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: 'latest java title something',
            body: 'latest body',

        }
    }
    componentDidMount() {
        var data = {
            title: this.state.title,
            body: this.state.body
        };

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            // headers: {'Content-Type':'application/x-www-form-urlencoded'},
            body: JSON.stringify(data)
        }).then((res) => res.json())
            .then((data) => console.log(data))
            .catch((err) => console.log(err))
    }
    render() {

        return (
            <div classname="App">

                <h2>done</h2>
            </div>
        );
    }
}
export default FetchPost;