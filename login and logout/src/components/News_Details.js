import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as  newsActions from '../store/actions/newsActions'
import { bindActionCreators } from 'redux';



class NewsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
        selectedNews: {}
        }
    }

    componentWillMount() {
        console.log ("i am will mount")
        this.props.newsActions.fetchNewsById(this.props.match.params.id)
    }

    componentWillReceiveProps(newProps) {
        if (newProps.newsItem1) {
            console.log (newProps.newsItem1)
            this.setState({
                selectedNews: newProps.newsItem1
            })
        }
    }


    render() {
        return (
            <React.Fragment>
                <div>
                    publishDate:{this.state.selectedNews.timestamp}
                </div>
                <div>
                    {this.state.selectedNews.body}
                </div>
            </React.Fragment>
        )
    }
}


function mapStateToProps(state) {
    console.log(state);
    debugger;
    return {
        newsItem1: state.newsReducer.newsItem
    };
}

function mapDispatchToprops(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToprops)(NewsDetails);
