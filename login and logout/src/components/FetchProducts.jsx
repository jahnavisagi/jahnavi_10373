import React, { Component } from 'react';
import './FetchProducts.css'
class DisplayAllProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
            .then(response => response.json())
            .then(productsData => {
                this.setState({
                    isLoaded: true,
                    products: productsData
                })
            });
    }
    render() {
        var { isLoaded, products } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>                
               <table align="center" class="table1" >
                   <caption><h2 style={{textalign:"center"}}>Product Details</h2></caption>
               <tr class="tr1">
                   <th class="th1">Product Id</th>
                   <th class="th1">Product Name</th>
                   <th class="th1">Price</th>
                   <th class="th1">Quantity</th>
                   <th class="th1">productType</th>
               </tr >
                    {products.map(product => (
                        <tr key="{product.id}" class="tr1">
                            <td> {product.product_id} </td> 
                            <td> {product.productname} </td>
                           <td> {product.price} </td>
                           <td> {product.quantity} </td>
                           <td>  {product.productType}</td>
                        </tr>
                    ))}
                </table>
            </div>
        );
    }
}
export default DisplayAllProducts;