import React, { Component } from 'react';
import './information.css'

// let news = [{ img: "https://aidforcancer.com/static/media/newsDefault.33e07ba7.jpg", text: "Working with NCI Divisions, Offices and Centers to Advance Global Cancer Research" },
// {img:"https://aidforcancer.com/static/media/newsDefault.33e07ba7.jpg",text:"For Early-Stage Lung Cancer, Study Identifies Potential New Biomarker, Treatment Target"},
// {img:"https://www.aidforcancer.com:8000/public/images/5b8969c109aa2626e4641eb71547027735241.jpg",text:"New molecule stops cancer from 'tricking' the immune system"},
// {img:"https://aidforcancer.com/static/media/newsDefault.33e07ba7.jpg",text:"Tata Trusts to Contribute ₹1,000 Crore to Treat Cancer Patients in Five States"},
// {img:"https://aidforcancer.com/static/media/newsDefault.33e07ba7.jpg ",text:"CPTAC and FDA Discuss Upcoming Computational Challenge in the Journal Nature Medicine"}]

// var fun = news.map(mul =>
//     <div>
//         <img  src={fun.img}></img>
//         <h5 class="textright">{fun.text}</h5>
//     </div>
//     )


class Information extends Component {
    render() {
        return (
            <div>
                <div class="container-fluid head1">
                    <nav class="navbar navbar-expand-md fixed-top head">
                        <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="navbar-brand" href="#">AID FOR CANCER</a>
                        <div class="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul class="nav navbar-nav navbar-right ml-auto nav-pills">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">information</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">opinions</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">forum</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">volunteer</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">seekhelp</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><button class="btn btn-info btn-md navbar-btn">Sign In</button></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10 col-xs-12 main">
                            <p style={{ color: "white" }}>Information about cancer</p>
                            <p style={{ color: "white", fontSize: 15 }}>Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div class="body">
                        <div class="row"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="col-sm-4 col-xs-12">
                                <nav class="navbar">
                                    <ul class="nav navbar-brand border" role="tablist">
                                        <li class="nav-item ">
                                            <a class="nav-link active" href="#" >News</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" >NGO'S/NPO'S</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" >Support group</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" >online help</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-sm-5">
                              {/* {fun} */}
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default Information;