import React, { Component } from 'react';
import './App.css';
import Posts from './components/post';
import PostForm from './components/postForm';

class App extends Component {
  render() {
    return (
      <div>
        <PostForm/>
        <hr/>
        <Posts/>
      </div>

    );
  }
}

export default App;
